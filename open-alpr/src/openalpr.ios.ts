import { ImageSource } from "tns-core-modules/image-source/image-source";

declare var NSObject: any;
declare var OAScannerDelegate: any;
declare var NSString: any;
declare var OAScanner: any;
declare var UIImage: any;
declare var interop: any;
declare type OAScannerDelegate = any;
declare var OAPlate: any;

export class OAScannerDelegateImpl extends NSObject implements OAScannerDelegate {

    public static ObjCProtocols = [OAScannerDelegate];

    static new(): OAScannerDelegateImpl {
        return <OAScannerDelegateImpl>super.new();
    }

    private _owner: WeakRef<OAScannerImpl>;

    public static initWithOwner(owner: WeakRef<OAScannerImpl>) {
        const instance = OAScannerDelegateImpl.new();
        instance._owner = owner;
        return instance;
    }

    public didScanResults(results): any {
        console.log("Results:::", results);
    }
    public didScanResultsJSON(results): any {
        console.log("ResultsJSON:::", results)
    }
    public didScanBestPlates(plates): any {
        console.log("BestPlateResults:::", plates)
    }
    public didScanPlateResult(result): any {
        console.log("PlateResult:::", result)
    }
    public didFailToLoadwithError(error): any {
        console.log("ErrorWhileLoading:::", error)
    }

}

export class OAScannerImpl extends OAScanner {

    public _owner: WeakRef<OpenALPR>;
    private delegate: any;

    public static new(): OAScannerImpl {
        return new OAScannerImpl();
    }

    public static initWithOwner(owner: WeakRef<OpenALPR>) {
        const instance = <OAScannerImpl>OAScannerImpl.new();
        instance._owner = owner;
        this.delegateRef = OAScannerDelegateImpl.initWithOwner(new WeakRef(instance));
        instance.delegate = this.delegateRef;
        return instance;
    }

    public initWithCountry(country: string, configFile: string, runDir: string): any {
        let self;
        if (configFile && runDir) {
            self = super.initWithCountry(country, configFile, runDir);
        } else if (configFile) {
            self = super.initWithCountry(country, configFile);
        } else {
            self = super.initWithCountry(country);
        }
        return self;
    }

    public scanImage(image: ImageSource): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                super.scanImageOnSuccessOnFailure(image.ios, function (res) {
                    resolve(res);
                }, function (error) {
                    reject(error);
                });
            }
            catch (e) {
                reject(e)
            }
        })
    }

    public scanImageAtPath(path: string): Promise<any> {
        return new Promise((resolve, reject) => {
            try {
                super.scanImageAtPathOnSuccessOnFailure(path, (res) => {
                    resolve(res);
                }, (error) => {
                    reject(error);
                });
            }
            catch (e) {
                reject(e)
            }
        })
    }

}

export class OpenALPR {

    private __OAScanner: OAScannerImpl;

    constructor(country: string, region: string) {
        this.__OAScanner = OAScannerImpl.initWithOwner(new WeakRef(this));
        if (this.__OAScanner) {
            this.__OAScanner.initWithCountry(country, region, undefined);
        }
    }

    public scanImage(image: ImageSource): Promise<any> {
        return new Promise((resolve, reject) => {
            if (image.ios) {
                this.__OAScanner.scanImage(image).then((res) => {
                    resolve(res);
                }, error => {
                    reject(error)
                });
            } else {
                reject(new Error("Invalid image"))
            }
        })
    }

    public scanImageAtPath(path: string): Promise<any> {
        return new Promise((resolve, reject) => {
            if (path) {
                this.__OAScanner.scanImageAtPath(path).then((res) => {
                    resolve(res);
                }, error => {
                    reject(error)
                });
            } else {
                reject(new Error("Invalid path to image"))
            }
        })
    }

}


export class OAPlateImpl extends OAPlate {


}



// function start(country, region, pathToImage) {

//     return new Promise(function (resolve, reject) {

//         var intent = new android.content.Intent(context, com.sandro.openalprsample.MainActivity.class);
//         // intent.setClassName("com.sandro.openalprsample", "com.sandro.openalprsample.MainActivity");

//         application.android.foregroundActivity.startActivity(intent);

//     })
// }

// var OAScannerDelegateImpl = (function (_super) {
//     __extends(OAScannerDelegateImpl, _super);
//     function OAScannerDelegateImpl() {
//         return _super !== null && _super.apply(this, arguments) || this;
//     }
//     OAScannerDelegateImpl.new = function () {
//         return _super.new.call(this);
//     };
//     OAScannerDelegateImpl.prototype.initWithCallback = function (owner, callback) {
//         this._owner = owner;
//         this._callback = callback;
//         return this;
//     };
//     OAScannerDelegateImpl.prototype.initWithCallbackAndOptions = function (owner, callback, options) {
//         this._owner = owner;
//         this._callback = callback;
//         if (options) {
//             this._width = options.width;
//             this._height = options.height;
//             this._keepAspectRatio = types.isNullOrUndefined(options.keepAspectRatio) ? true : options.keepAspectRatio;
//         }
//         else {
//             this._keepAspectRatio = true;
//         }
//         return this;
//     };
//     OAScannerDelegateImpl.prototype.createDateFromString = function (value) {

//     };
//     OAScannerDelegateImpl.prototype.qb_imagePickerControllerDidFinishPickingAssets = function (picker, assets) {

//     };
//     OAScannerDelegateImpl.prototype.qb_imagePickerControllerDidCancel = function (picker) {
//         this._owner.get().closePicker();
//     };
//     OAScannerDelegateImpl.ObjCProtocols = [QBImagePickerControllerDelegate];
//     return OAScannerDelegateImpl;
// }(NSObject));
// exports.OAScannerDelegate = OAScannerDelegate;



// function start(country, region, pathToImage) {// let imagePath = Bundle.main.path("dk_vb33742", "jpg")
//     return new Promise((reslove, reject) => {
//         console.log("doesFun:::", OAScanner)
//         console.log("doesFunInit:::", OAScanner.initWithCountry)
//         // console.log("doesFun:::", OAScanner)
//         if (OAScanner.initWithCountry) {
//             var alprScanner = OAScanner.initWithCountry(country, region)
//         }
//         // let alprScanner = OAScanner(country, region)
//         console.log("doesFunScan:::", alprScanner.scanImage)
//         alprScanner.scanImage(pathToImage, (plates) => {
//             reslove(plates)
//             // plates.forEach((plate) => {
//             //     console.log('Plate:::', plate.number);
//             // })
//         }, error => {
//             reject(error)
//             console.log('Error:::', errorF)
//         })
//     })
// }
// exports.start = start;