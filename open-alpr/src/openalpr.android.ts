import { ImageSource } from "tns-core-modules/image-source/image-source";

export class OpenALPR {


    constructor(country: string, region: string) {

    }

    public scanImage(image: ImageSource): Promise<any> {
        return new Promise((resolve, reject) => {
            console.log('Scanning')
            resolve({ result: 'Scanned' });
        })
    }

    public scanImageAtPath(path: string): Promise<any> {
        return new Promise((resolve, reject) => {
            console.log('Scanning')
            resolve({ result: 'Scanned' });
        })
    }
}