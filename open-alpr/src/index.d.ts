import { ImageSource } from "tns-core-modules/image-source/image-source";



export declare class OpenALPR {
    constructor(country: string, region: string);
    public scanImage(image: ImageSource): Promise<any>;
    public scanImageAtPath(path: string): Promise<any>;
}
// function start(country, region, pathToImage) {// let imagePath = Bundle.main.path("dk_vb33742", "jpg")
//     return new Promise((reslove, reject) => {
//         try {
//             var alpr = OAScanner(country, region);
//             // var rr = alpr.init();
//             console.log("Alpr:::", alpr)
//             // console.log("Alpr:::", rr.scanImage())
//             // var ref = new interop.Pointer(alpr);
//             console.log("Ref:::", alpr.scanImageAtPath)
//             // console.log("Ref:::", ref.value)
//             // console.log("Ref:::", ref.value.scanImage)
//             rr.scanImageAtPath(pathToImage, function res(response) {
//                 console.log('res:', response)
//             }, function res(response) {
//                 console.log('res:', response)
//             })
//         }
//         catch (e) {
//             console.log("Failed1::", e)
//             try {
//                 var alpr = OAScanner();
//                 // var rr = alpr.init();
//                 console.log("Alpr:::", alpr)
//                 // console.log("Alpr:::", rr.scanImage())
//                 // var ref = new interop.Pointer(alpr);
//                 console.log("Ref:::", alpr.initWithCountry(country, region))
//                 // console.log("Ref:::", ref.value)
//                 // console.log("Ref:::", ref.value.scanImage)
//                 rr.scanImageAtPath(pathToImage, function res(response) {
//                     console.log('res:', response)
//                 }, function res(response) {
//                     console.log('res:', response)
//                 })
//             }
//             catch (t) {
//                 console.log("Failed2::", t)
//                 try {
//                     var alpr = OAScanner().alloc().init();
//                     // var rr = alpr.init();
//                     console.log("Alpr:::", alpr)
//                     // console.log("Alpr:::", rr.scanImage())
//                     // var ref = new interop.Pointer(alpr);
//                     console.log("Ref:::", alpr.initWithCountry(country, region))
//                     console.log("Ref:::", alpr.delegate)
//                     // console.log("Ref:::", ref.value)
//                     // console.log("Ref:::", ref.value.scanImage)
//                     rr.scanImageAtPath(pathToImage, function res(response) {
//                         console.log('res:', response)
//                     }, function res(response) {
//                         console.log('res:', response)
//                     })
//                 }
//                 catch (u) {
//                     console.log("Failed3::", u)
//                     try {
//                         var alpr = new OAScanner();
//                         // var rr = alpr.init();
//                         console.log("Alpr:::", alpr)
//                         // console.log("Alpr:::", rr.scanImage())
//                         // var ref = new interop.Pointer(alpr);
//                         var newIns = alpr.initWithCountry(country);
//                         console.log("Ref:::", alpr.delegate)
//                         console.log("Ref:::", newIns.setPatternRegion)
//                         // alpr.initWithCountry(country)
//                         newIns.setPatternRegion(region);
//                         console.log("Ref:::", newIns.scanImageAtPath)
//                         console.log("Ref:::", newIns.setDetectRegion)
//                         newIns.scanImageAtPath(pathToImage, function res(response) {
//                             console.log('res:', response)
//                         }, function res(response) {
//                             console.log('res:', response)
//                         })
//                     }
//                     catch (m) {
//                         console.log("Failed4::", m)
//                     }
//                 }
//             }
//         }

//     })
// }

// import { OpenALPR } from "./openalpr.ios"

// exports.OpenALPR = OpenALPR;