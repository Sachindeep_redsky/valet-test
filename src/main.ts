import { platformNativeScriptDynamic } from "nativescript-angular/platform";
import { enableProdMode } from "@angular/core";
import { AppModule } from "./app/app.module";
import * as firebase from "nativescript-plugin-firebase";

firebase.init();

enableProdMode();
platformNativeScriptDynamic().bootstrapModule(AppModule);
