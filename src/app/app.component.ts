import { Component, ViewChild, ElementRef, OnInit, AfterViewInit } from "@angular/core";
import { ConnectionService } from './services/connection.service';
import { ApiService } from './services/api.service';
import { Folder, path, knownFolders } from 'tns-core-modules/file-system/file-system';
import { isAndroid, isIOS } from "tns-core-modules/ui/page/page";
import { UploadService } from "./services/upload.service";
import { LogGuardService } from "./services/log.guard.service";
import { NavigationService } from "./services/navigation.service";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { RouterExtensions } from "nativescript-angular/router/router-extensions";
import { Values } from "./values/values";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { FingerprintAuth, BiometricIDAvailableResult } from "nativescript-fingerprint-auth";
import * as Permissions from "nativescript-permissions";
import * as application from 'application';
import { registerElement } from "nativescript-angular/element-registry";
import { initializeOnAngular } from 'nativescript-image-cache';
registerElement("FilterableListpicker", () => require("nativescript-filterable-listpicker").FilterableListpicker);


@Component({
    selector: "ns-app",
    moduleId: module.id,
    templateUrl: "./app.component.html"
})

export class AppComponent implements OnInit {

    @ViewChild('rad', { static: false }) radDrawer: ElementRef;
    private _rad: RadSideDrawer;
    private fingerprintAuth: FingerprintAuth;

    connectionStatusText: string;
    labelBackground: string;

    constructor(private connectionService: ConnectionService, private routerExtensions: RouterExtensions, private apiService: ApiService, private uploadService: UploadService, private navigationService: NavigationService, private logGuardService: LogGuardService, private http: HttpClient) {

        this.navigationService.setCurrentPage("App")
        this.connectionStatusText = '';
        // this.tempImageRemover();
        // this.fingerprintAuth = new FingerprintAuth();

        // this.fingerprintAuth.available().then((result: BiometricIDAvailableResult) => {
        //     console.log(`Biometric ID available? ${result.any}`);
        //     console.log(`Touch? ${result.touch}`);
        //     console.log(`Face? ${result.face}`);

        //     if (!result) {
        //         return;
        //     }

        //     this.fingerprintAuth.didFingerprintDatabaseChange().then(changed => {
        //         if (changed) {
        //             this.routerExtensions.navigate(["/login"]);
        //         } else {
        //             this.fingerprintAuth.verifyFingerprint(
        //                 {
        //                     title: "Enter your password",
        //                     message: "Scan yer finger", // optional
        //                     authenticationValidityDuration: 10 // Android
        //                 })
        //                 .then((enteredPassword?: string) => {


        //                     console.log('Verified::;')
        //                 })
        //                 .catch(err => console.log(`Biometric ID NOT OK: ${JSON.stringify(err)}`));
        //         }
        //     });
        // }, error => {
        //     console.log("ERR::", error);
        // });

        this.connectionService.connectionChanges.subscribe((state: boolean) => {
            if (state) {
                // console.log("Connected")
                this.connectionStatusText = "Connected"
                this.labelBackground = "green"
                
            }
            else {
                // console.log("Not Connected")
                this.connectionStatusText = "Internet Disconnected";
                this.labelBackground = "black";
                alert(this.connectionStatusText);
            }
        })
        
        this.connectionService.connectionTextChanges.subscribe((text: string) => {
            this.connectionStatusText = text;
        },
            error => {

            });



        application.on(application.discardedErrorEvent, function (args) {
            const error = args.error;

            console.log("Received discarded exception: ");
            console.log(error.message);
            console.log(error.stackTrace);
            console.log(error.nativeException);
            //report the exception in your analytics solution here
        });


        application.on(application.suspendEvent, (args) => {
            if (args.android) {
                // For Android applications, args.android is an android activity class.
                this.connectionService.stopMoniterConnection();
                console.log("Activity: " + args.android);
            } else if (args.ios) {
                // For iOS applications, args.ios is UIApplication.
                this.connectionService.stopMoniterConnection();
                console.log("UIApplication: " + args.ios);
            }
        });

        if (isAndroid) {
            var permissions = [];
            permissions.push(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissions.push(android.Manifest.permission.READ_EXTERNAL_STORAGE);
            permissions.push(android.Manifest.permission.CAMERA);

            Permissions.requestPermissions(permissions, "Needed for creating Files").then(() => {
                console.log("Permission granted!");
            }).catch(() => {
                console.log("Permission is not granted");
            });
        }

        // Permissions.requestPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE, "Needed for getting Files").then(() => {
        //     console.log("Permission granted!");
        // }).catch(() => {
        //     console.log("Permission is not granted");
        // })

    }

    ngOnInit(): void {
        initializeOnAngular();

        if (Values.readString(Values.AUTHERIZATION_TOKEN, "") != "") {
            this.routerExtensions.navigate(["/dashboard"]);
        }
        else {
            this.routerExtensions.navigate(["/tenant"]);
        }

    }
 
    async onLogout() {
        // Values.writeString(Values.AUTHERIZATION_TOKEN, "");

        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, "")
        });

        await this.http.post(Values.BASE_URL + "/api/logout", {}, { headers: headers }).subscribe((res: any) => {
            this.routerExtensions.navigate(["/login"]);
        }, error => {
            this.routerExtensions.navigate(["/login"]);
        })

        this.logGuardService.logout();
        try {
            this._rad.closeDrawer();
        }
        catch {
            console.log("Found some error")
        }
    }


    tempImageRemover() {

        var folder;
        if (isAndroid) {
            folder = Folder.fromPath("/storage/emulated/0/Valet/Pending/Signatures");
        } else if (isIOS) {
            folder = Folder.fromPath(path.join(knownFolders.documents().path));
        }
        // var folder = Folder.fromPath("/storage/emulated/0/Valet/Pending/Signatures");

        folder.getEntities().then((res) => {
            if (res != undefined && res != null && res.length != 0) {
                for (var i = 0; i < res.length; i++) {
                    var fileName = res[i].name;
                    var fileNameArray = fileName.split("_")

                    if (fileNameArray != undefined && fileNameArray != null && fileNameArray.length != 0) {
                        if (fileNameArray[0] != undefined) {
                            if (fileNameArray[0] == "PS") {
                                res[i].remove();
                            }
                        }
                    }
                    // console.log("Index" + i, "Name:", res[i]);
                }
            }
        },
            error => {

            })

    }

}
