export class CheckInOut {

    date: string
    timezone_type: number
    timezone: string

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.date = obj.date;
        this.timezone_type = obj.timezone_type;
        this.timezone = obj.timezone;

    }
}