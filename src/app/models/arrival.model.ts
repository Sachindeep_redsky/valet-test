export class Arrival {

    airline_id: number;
    arrival_at: string;
    flight_number: string;

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.airline_id = obj.airline_id;
        this.arrival_at = obj.arrival_at;
        this.flight_number = obj.flight_number;
    }
}