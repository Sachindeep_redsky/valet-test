export class Damage {

    id: number;
    position: string;
    position_type: string;
    image_data: string;
    recorded_on: string;
    //Not to be posted
    image_path: string;
    booking_id: number;
    checking_status: string

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.id = obj.id;
        this.position = obj.position;
        this.position_type = obj.position_type;
        this.image_data = obj.image_data;
        this.recorded_on = obj.recorded_on;
        this.image_path = obj.image_path;
        this.booking_id = obj.booking_id;
        this.checking_status = obj.checking_status;
    }
}