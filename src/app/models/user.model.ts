export class User {

    name: string;
    mobile_no: string;
    email: string;
    password: string;

    constructor(obj?: any) {

        if (!obj) {
            return;
          }
          
        this.name = obj.name;
        this.mobile_no = obj.mobile_no;
        this.email = obj.email;
        this.password = obj.password;

    }
}