export class CheckInOutPost {

    signature: string;

    //Not to be posed
    checking_status: string;
    booking_id: number;

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.signature = obj.signature;
    }
}