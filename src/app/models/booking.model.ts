import { Departure } from "./departure.model";
import { Arrival } from "./arrival.model";
import { CarInfo } from "./car-info.model";
import { User } from "./user.model";
import { Airport } from './airport.model';
import { CheckInOut } from './check-in-out.model';
import { OptionalServices } from './optionalservices.model';

export class Booking {

    id: number;
    airport: Airport;
    has_arrival: boolean;
    departure: Departure;
    arrival: Arrival;
    car_info: CarInfo;
    checkin: CheckInOut;
    checkout: CheckInOut;
    user_info: User;
    expected_time: number;
    optionalServices: OptionalServices;
    parking_service: OptionalServices;
    airport_id : any;

    constructor(obj?: any) {
        if (!obj) {
            return;
        }

        this.id = obj.id;
        this.airport = obj.airport;
        this.has_arrival = obj.has_arrival;
        this.departure = obj.departure;
        this.arrival = obj.arrival;
        this.car_info = obj.carInfo;
        this.checkin = obj.checkin;
        this.checkout = obj.checkout;
        this.user_info = obj.user;
        this.expected_time = obj.expected_time
        this.parking_service = obj.parking_service
    }
}