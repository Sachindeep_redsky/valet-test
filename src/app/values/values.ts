import * as appSettings from "application-settings"
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { Damage } from "../models/damage.model";

export class Values {

    public static BASE_URL: string = "https://test1.staging.valetcloud.clouderpro.com";
    public static AUTHERIZATION_TOKEN: string = "autherization_token";
    public static PENDING_DAMAGES: string = "pendingDamages";
    public static ENCRYPTION_KEY: string = "encryptionKey";
    public static PENDING_CHECKS: string = "pendingChecks";
    public static DAMAGE_PATHS: string = "damagePaths";
    public static OPTIONAL_SERVICES: string = "optionalServices";
    public static CACHED_BOOKING_AGENDA: string = "cachedBookingAgenda";
    public static SELECTED_AIRLINE: string = "airlinesId";
    public static SELECTED_LOCATION: string = "location";
    public static APP_BACKGROUND_IMAGE: string = "appbackgroundimage";
    public static APP_LOGO: string = "applogo";
    public static APP_THEME: string = "apptheme";

    public static writeString(key: string, value: string): void {
        appSettings.setString(key, value);
    }

    public static readString(key: string, defaultValue: string): string {
        return appSettings.getString(key, defaultValue);
    }

    public static writeNumber(key: string, value: number): void {
        appSettings.setNumber(key, value);
    }

    public static readNumber(key: string, defaultValue: number): number {
        return appSettings.getNumber(key, defaultValue);
    }

    public static writeBoolean(key: string, value: boolean): void {
        appSettings.setBoolean(key, value);
    }

    public static readBoolean(key: string, defaultValue: boolean): boolean {
        return appSettings.getBoolean(key, defaultValue);
    }

    public static doesExist(key: string): boolean {
        return appSettings.hasKey(key);
    }

    public static remove(key: string) {
        appSettings.remove(key);
    }


    // public static writeToDamageList(damageObj) {
    //     var temp;
    //     var storageString = Values.readString(Values.PENDING_DAMAGES, "");
    //     if (storageString != "") {
    //         var arr = [];
    //         temp = new ObservableArray<Damage>();
    //         arr = JSON.parse(storageString);
    //         for (var i = 0; i < arr.length; i++) {
    //             temp.push(new Damage(arr[i]))
    //         }
    //     }
    //     else {
    //         temp = new ObservableArray<Damage>();
    //     }

    //     temp.push(new Damage(damageObj))

    //     console.trace("OBBB:", temp._array)

    //     Values.writeString(Values.PENDING_DAMAGES, JSON.stringify(temp._array))
    //     console.trace("Written_Dam", Values.readString(Values.PENDING_DAMAGES, ""))

    // }

}