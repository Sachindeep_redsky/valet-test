import { NgModule, NO_ERRORS_SCHEMA, InjectionToken } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { DashboardRoutingModule } from "./dashboard-routing.module";
import { DashboardComponent } from "./components/dashboard.component";
import { NativeScriptCommonModule } from 'nativescript-angular/common';
import { NgModalModule } from "../modals/ng-modal";
// import { HeaderListComponent } from "../headerList/header-list.component";

@NgModule({
  imports: [
    DashboardRoutingModule,
    NgModalModule,
    NativeScriptCommonModule,
    NativeScriptFormsModule,
    NativeScriptHttpClientModule,
  ],
  declarations: [DashboardComponent],
  schemas: [NO_ERRORS_SCHEMA]
})
export class DashboardModule { }
