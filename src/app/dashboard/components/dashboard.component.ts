import { Component, OnInit, ViewChild } from "@angular/core";
import { Page, Observable, EventData } from "tns-core-modules/ui/page/page";
import { setCurrentOrientation } from "nativescript-screen-orientation";
import { TextField } from "tns-core-modules/ui/text-field";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { RouterExtensions } from "nativescript-angular/router";
import { ExtendedNavigationExtras } from "nativescript-angular/router/router-extensions";
import { ObservableArray } from "tns-core-modules/data/observable-array/observable-array";
import { isAndroid, isIOS } from "platform/platform";
import { ConnectionService } from "~/app/services/connection.service";
import { NavigationService } from "~/app/services/navigation.service";
import { Menu } from "nativescript-menu";
import { DatePicker } from "tns-core-modules/ui/date-picker/date-picker";
import { ModalComponent } from "~/app/modals/modal.component";
import { requestPermissions, takePicture, isAvailable } from "nativescript-camera";
import { fromAsset } from "tns-core-modules/image-source";
import * as moment from "moment/moment";
import * as ImageSourceModule from "tns-core-modules/image-source/";
// var openalpr;

// if (isIOS) {
//   openalpr = require("nativescript-openalpr/openalpr.ios")
// } else {
//   openalpr = require("nativescript-openalpr/openalpr.android")
// }


@Component({
  selector: "app-dashboard",
  moduleId: module.id,
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})

export class DashboardComponent implements OnInit {
  appTheme: any;
  redColor: string;
  blueColor: string;
  grayColor: string;
  whiteColor: string;
  checkInIcon: string;
  checkOutIcon: string;
  clearFilterIcon: string;
  addCarIcon: string;
  airportName: string;
  @ViewChild('datepickerDialog', { static: false }) datePickerModal: ModalComponent;
  @ViewChild('scannedList', { static: false }) scannedListModal: ModalComponent;

  sourceDataItems;
  items;
  viewModel;
  actions;

  scannedPlateResults;
  optionalServices;

  date: string;
  customerName: string;
  carPlate: string;
  flightNumber: string;
  baseUrl: string;
  selectedList: string;
  canExit: boolean;
  isNavigating: boolean;
  isLoading: boolean;
  hasBeenHitOnce: boolean;
  hasBeenCachedOnce: boolean;
  pageRef: Page;
  showDatePicker: boolean;
  selectedDate: string;
  airlinesId: string;

  currentDate: Date;

  isRendering: boolean;
  renderViewTimeout;



  upItems = [{
    "name": "Car wash option 1 - small cars",
    "amount": 100,
    "code": "CW-1-1",
    "items": [
      {
        "name": "Car wash option 1 - medium cars",
        "code": "CW-1-3",
        "amount": 8000
      },
      {
        "name": "Car wash option 2 - big cars",
        "code": "CW-2-3",
        "amount": 13000
      }
    ]
  },
  {
    "name": "Car wash option 2 - small cars",
    "amount": 120,
    "code": "CW-1-1",
    "items": [
      {
        "name": "Car wash option 1 - medium cars",
        "code": "CW-1-3",
        "amount": 8000
      },
      {
        "name": "Car wash option 2 - big cars",
        "code": "CW-2-3",
        "amount": 13000
      }
    ]
  },
  {
    "name": "Car wash option 3 - small cars",
    "amount": 90,
    "code": "CW-1-1",
    "items": [
      {
        "name": "Car wash option 1 - medium cars",
        "code": "CW-1-3",
        "amount": 8000
      },
      {
        "name": "Car wash option 2 - big cars",
        "code": "CW-2-3",
        "amount": 13000
      }
    ]
  }]


  constructor(private http: HttpClient, private routerExtensions: RouterExtensions, private connectionService: ConnectionService, private navigationService: NavigationService) {
    if (isAndroid) {
      setCurrentOrientation("landscape", function () {
        console.log("set landscape orientation");
      });
    }
    this.navigationService.setCurrentPage("Dashboard")
    this.refreshTheme()
    this.sourceDataItems = [];
    this.scannedPlateResults = [];
    this.optionalServices = [];
    this.items = new ObservableArray();
    this.date = "";
    this.customerName = "";
    this.carPlate = "";
    this.flightNumber = "";
    this.baseUrl = '';
    this.selectedList = "Ready to Check-In or Check-out";
    this.canExit = false;
    this.isRendering = false;
    this.isNavigating = false;
    this.isLoading = false;
    this.hasBeenHitOnce = false;
    this.hasBeenCachedOnce = false;
    this.showDatePicker = false;
    this.currentDate = new Date();
    this.selectedDate = this.currentDate.getDate() + '-' + (this.currentDate.getMonth() + 1) + '-' + this.currentDate.getFullYear();
    this.actions = [];
    this.actions.push("Ready to Check-In or Check-out");
    this.actions.push("Complete");

    this.onSearchTap();
    // this.getOptionalServices()
  }

  ngOnInit(): void {

    if (isAndroid) {
    }

    if (Values.readString(Values.SELECTED_LOCATION, '') == "") {
      this.routerExtensions.navigate(['login']);
      alert('Please select a location');
    } else {
      let airline = JSON.parse(Values.readString(Values.SELECTED_LOCATION, ''))
      console.log(airline)
      this.airlinesId = airline.id;
    }
    this.renderViewTimeout = setTimeout(() => {
      this.isRendering = true;
    }, 200)


    this.connectionService.connectionChanges.subscribe((state) => {
      if (state == true && this.hasBeenHitOnce == false) {
        this.onSearchTap();
        this.hasBeenHitOnce = true;
      }
      else if (state == false && this.hasBeenCachedOnce == false) {
        var cachedBookingAgendaString = Values.readString(Values.CACHED_BOOKING_AGENDA, "")
        this.isLoading = true;

        if (cachedBookingAgendaString != "") {

          var data = JSON.parse(cachedBookingAgendaString)
          this.sourceDataItems = [];

          for (var i = 0; i < data.length; i++) {

            if ((data[i].checkin == null && data[i].checkout == null) || (data[i].checkin != null && data[i].checkout == null)) {
              if (this.selectedList == this.actions[0]) {
                this.sourceDataItems.push(data[i]);
              }
            }
            if ((data[i].checkin != null && data[i].checkout != null) || (data[i].checkin == null && data[i].checkout != null)) {
              if (this.selectedList == this.actions[1]) {
                this.sourceDataItems.push(data[i]);
              }
            }
          }

        }
        else {
          this.sourceDataItems = [];
        }
        this.viewModel = new Observable();
        this.viewModel.set("items", this.sourceDataItems);
        this.hasBeenCachedOnce = true;
        setTimeout(() => {
          this.isLoading = false;
        }, 400)
      }
    })
  }

  onPageLoaded(args: EventData) {
    this.pageRef = args.object as Page;
  }


  onPickerLoaded(args) {
    let datePicker = <DatePicker>args.object;

    var date = new Date();

    datePicker.year = date.getFullYear();
    datePicker.month = date.getMonth() + 1;
    datePicker.day = date.getDate();
    datePicker.minDate = new Date(1975, 0, 29);
    datePicker.maxDate = new Date(2045, 4, 12);
  }

  onDateChanged(args) {
    var date = new Date(args.value);
    console.log('DATE:::', date)
    this.selectedDate = date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
    this.currentDate = date;
  }

  public openDatePicker(args) {
    this.datePickerModal.show();
  }

  public pickDate() {
    this.datePickerModal.hide();
  }

  public openScannedList() {
    // this.scannedListModal.show();
  }

  public closeScannedList() {
    // this.scannedListModal.hide();
  }

  onInterchangePress() {
    Menu.popup({
      view: this.pageRef.getViewById("interchangeButton"),
      actions: this.actions
    }).then((value: any) => {
      console.log("Value:", value)
      var tempValue;

      console.log("Type:", typeof (value))

      if (typeof (value) == 'string') {
        tempValue = value;
      } else if (typeof (value) == 'object') {
        if (value != undefined && value.title != undefined) {
          tempValue = value.title;
        }
      }

      this.selectedList = tempValue;
      if (this.connectionService.isConnected) {
        this.onSearchTap();
      } else {
        var cachedBookingAgendaString = Values.readString(Values.CACHED_BOOKING_AGENDA, "")
        this.sourceDataItems = [];
        this.isLoading = true;

        if (cachedBookingAgendaString != "") {

          var data = JSON.parse(cachedBookingAgendaString)
          for (var i = 0; i < data.length; i++) {

            if ((data[i].checkin == null && data[i].checkout == null) || (data[i].checkin != null && data[i].checkout == null)) {
              if (this.selectedList == this.actions[0]) {
                this.sourceDataItems.push(data[i]);
              }
            }
            if ((data[i].checkin != null && data[i].checkout != null) || (data[i].checkin == null && data[i].checkout != null)) {
              if (this.selectedList == this.actions[1]) {
                this.sourceDataItems.push(data[i]);
              }
            }
          }
        }
        else {
          this.sourceDataItems = [];
        }
        this.viewModel = new Observable();
        this.viewModel.set("items", this.sourceDataItems);

        setTimeout(() => {
          this.isLoading = false;
        }, 400)
        this.hasBeenCachedOnce = true;
      }
    });
  }

  public nameTextField(args) {
    var customerName = <TextField>args.object;
    this.customerName = customerName.text;
  }

  public carPlateTextField(args) {
    var carPlate = <TextField>args.object;
    this.carPlate = carPlate.text;
  }

  public flightNumberTextField(args) {
    var flightNumber = <TextField>args.object;
    this.flightNumber = flightNumber.text;
  }

  public dateTextField(args) {
    var date = <TextField>args.object;
    this.date = date.text;
  }

  public onItemTap(args) {
    this.isNavigating = true;
    if (this.sourceDataItems[args.index] != undefined) {
      let extendedNavigationExtras: ExtendedNavigationExtras = {
        queryParams: {
          "booking": JSON.stringify(this.sourceDataItems[args.index])

        },
      };

      setTimeout(() => {
        this.isNavigating = false;
      }, 800)
      this.routerExtensions.navigate(["/check"], extendedNavigationExtras)
    }
  }

  getTime(utcTime: number) {
    return moment(utcTime * 1000).format('HH:mm')
  }

  async onSearchTap() {

    let customerNameSearch = this.customerName.toLowerCase();
    let carPlateSearch = this.carPlate.toLowerCase();
    let flightNumberSearch = this.flightNumber.toLowerCase();
    let selectedLocation = Values.readString(Values.SELECTED_LOCATION, '');
    let locationObj;
    let query = 'airport_id=2660644';
    if (selectedLocation) {
      locationObj = JSON.parse(selectedLocation);
      query = `airport_id=${locationObj.id}`
      this.airportName = locationObj.name;
    }
    if (query != '') {
      query = "?" + query
    }
    if (customerNameSearch != '' && customerNameSearch != null && customerNameSearch != undefined) {
      query = query + "&" + "userinfo_name=" + customerNameSearch;
    }

    if (carPlateSearch != '' && carPlateSearch != null && carPlateSearch != undefined) {
      if (query != '') {
        query = query + "&" + "license_plate=" + carPlateSearch;
      }
      else {
        query = query + "license_plate=" + carPlateSearch;
      }
    }

    if (flightNumberSearch != '' && flightNumberSearch != null && flightNumberSearch != undefined) {
      if (query != '') {
        query = query + "&" + "flight_number=" + flightNumberSearch;
      }
      else {
        query = query + "flight_number=" + flightNumberSearch;
      }
    }

    // if (this.currentDate != null && this.currentDate != undefined) {

    //   let day: string = this.currentDate.getDate() < 10 ? '0' + this.currentDate.getDate() : '' + this.currentDate.getDate()
    //   let month: string = (this.currentDate.getMonth() + 1) < 10 ? '0' + (this.currentDate.getMonth() + 1) : '' + (this.currentDate.getMonth() + 1);
    //   //let date: string = month + '%2F' + day + '%2F' + this.currentDate.getFullYear();
    //   let date: string = this.currentDate.getFullYear() + '%2F' + month + '%2F' + day;

    //   if (query != '') {
    //     query = query + "&date=" + date;
    //   }
    //   else {
    //     query = query + "date=" + this.date;
    //   }
    // }


    this.isLoading = true;

    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, "")
    });
    this.isLoading = true;

    console.log("Query::::", query)

    // console.log("Token:", Values.readString(Values.AUTHERIZATION_TOKEN, ""))

    await this.http.get(Values.BASE_URL + "/api/bookings" + query, { headers: headers }).subscribe((res: any) => {
      // this.sourceDataItems = res.data;
      Values.writeString(Values.CACHED_BOOKING_AGENDA, JSON.stringify(res.data));
      // this.sourceDataItems = undefined;
      this.sourceDataItems = [];
      console.trace('Dashboardrespones', res)

      // if(res.status==401){

      // }

      for (var i = 0; i < res.data.length; i++) {

        if ((res.data[i].checkin == null && res.data[i].checkout == null) || (res.data[i].checkin != null && res.data[i].checkout == null)) {
          if (this.selectedList == this.actions[0]) {
            this.sourceDataItems.push(res.data[i]);
          }
        }
        if ((res.data[i].checkin != null && res.data[i].checkout != null) || (res.data[i].checkin == null && res.data[i].checkout != null)) {
          if (this.selectedList == this.actions[1]) {
            this.sourceDataItems.push(res.data[i]);
          }
        }
      }

      this.viewModel = new Observable();
      this.viewModel.set("items", this.sourceDataItems);

      setTimeout(() => {
        this.isLoading = false;
      }, 400)

    }, error => {
      console.log('Error:::', error);
      setTimeout(() => {
        this.isLoading = false;
      }, 400)
      if (error.error.message == 'Unauthenticated.') {
        alert('Session Expired, Please login again')
        setTimeout(() => {
          Values.writeString(Values.AUTHERIZATION_TOKEN, '');
          this.routerExtensions.navigate(['/login'])
        }, 400)
      }
    })
  }

  onClearFilters() {
    this.selectedList = "Ready to Check-In or Check-out";
    this.customerName = '';
    this.carPlate = '';
    this.flightNumber = '';
    this.onSearchTap()
  }
  onGetCheckInList(){
    this.selectedList = "Ready to Check-In or Check-out";

    this.onSearchTap();

  }
  onGetCheckOutList(){
    this.selectedList = "Complete";
    this.onSearchTap()
    
  }
  //Optional Services
  public onTextRecognitionResult(args) {
    console.trace(args.value)
  }

  ngOnDestroy() {
    clearTimeout(this.renderViewTimeout);
  }

  public getOptionalServices() {

    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, "")
    });

    this.http.get(Values.BASE_URL + "/api/services?airport_id=2660644", { headers: headers }).subscribe((res: any) => {
      this.optionalServices = res.data;
      Values.writeString(Values.OPTIONAL_SERVICES, JSON.stringify(res.data))
    }, error => {
      console.trace('error:::', error)
      Values.writeString(Values.OPTIONAL_SERVICES, '')
    })

  }

  onScanTap() {
    if (isIOS) {
      var openalpr = new openalpr.OpenALPR("eu", "");

      requestPermissions().then(success => {
        console.log('Got Permissions:::')
        if (isAvailable()) {
          takePicture().then(success => {
            fromAsset(success).then(image => {
              openalpr.scanImage(image).then((res) => {
                console.log("BestResultsResponse:", res);
                for (var i = 0; i < res.count; i++) {
                  this.scannedPlateResults.push({ "number": res[i].number, "confidence": res[i].confidence });
                }
                if (this.scannedPlateResults.length > 0) {
                  this.openScannedList();
                }
              }, error => {
                console.log("Failed to get results:", error);
                alert("Scanning failed, Please try again")
              });
            }, error => {
              console.log('Could not get picture from assets:::', error)
              alert("Could not get picture, Please try again")
            })
          }, failed => {
            console.log('Could not take picture:::', failed)
            alert("Could not take picture, Please try again")
          })
        }
        //dev purpose

        // fromUrl("http://3.0.96.134:2000/files/images/v5-1581426603047.png").then(image => {
        //   openalpr.scanImage(image).then((res) => {
        //     console.log("BestResultsResponse:", res);
        //     for (var i = 0; i < res.count; i++) {
        //       this.scannedPlateResults.push({ "number": res[i].number, "confidence": res[i].confidence });
        //     }

        //     if (this.scannedPlateResults.length > 0) {
        //       console.log('ScannedList:::', this.scannedPlateResults)
        //       this.openScannedList();
        //     }

        //   }, error => {
        //     console.log("Failed to get results:", error);
        //     alert("Scanning failed, Please try again")
        //   });
        // }, failed => {
        //   console.log('Could not download picture:::', failed)
        // }) 

      }, failed => {
        console.log('Failed to got Permissions:::', failed);
        alert("Could not get required permissions, Please try again")
      })
      // console.log('func:::', OpenALPR)
      // var openalpr = new OpenALPR("eu", "");
      // console.log('inst:::', openalpr);
      // try {
      //   openalpr.scanImageAtPath("./")
      // }
      // catch (e) {
      //   console.error("Error:::", e)
      // }

      // openalpr.start("eu", "", "/storage/emulated/0/OpenAlpr/v1.png").then(res => {
      //   console.log("RES:::", res)
      // }, error => {
      //   console.log("Error:::", error);
      // })}
    }
  }




  refreshTheme() {
    let Theme = Values.readString(Values.APP_THEME, "");
    if (Theme) { 
      this.appTheme = JSON.parse(Theme);

      if (this.appTheme && this.appTheme.theme && this.appTheme.theme.colors) {
        this.redColor = this.appTheme.theme.colors.one;
        this.blueColor = this.appTheme.theme.colors.two;
        this.grayColor = this.appTheme.theme.colors.bg1;
        this.whiteColor = this.appTheme.theme.colors.bg2;
      }
      else {
        this.redColor = '#f72231';
        this.blueColor = '#1896d2';
        this.grayColor = '#4d4d4d';
        this.whiteColor = '#eaeaea';
      }
      if (this.appTheme && this.appTheme.theme && this.appTheme.theme.btns) {
        this.checkInIcon = this.appTheme.theme.btns["check-in"];
        this.checkOutIcon = this.appTheme.theme.btns["check-out"];
        this.clearFilterIcon = this.appTheme.theme.btns.clear;
        this.addCarIcon = this.appTheme.theme.btns.add;
      }
      else {
      }
    }
    else {
      this.redColor = '#ffd444';
      this.blueColor = '#1896d2';
      this.whiteColor = '#eaeaea';
    }
  }

  onCreatebooking() {
    this.routerExtensions.navigate(["/createbooking"]);

  }

  onLogout() {
    // Values.writeString(Values.AUTHERIZATION_TOKEN, "");

    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, "")
    });

    this.http.post(Values.BASE_URL + "/api/logout", {}, { headers: headers }).subscribe((res: any) => {
      this.routerExtensions.navigate(["/login"], { clearHistory: true });
    }, error => {
      this.routerExtensions.navigate(["/login"], { clearHistory: true });
    })

  }
}
