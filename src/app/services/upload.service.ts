import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Values } from '../values/values'
import { ApiService } from './api.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { async } from 'rxjs/internal/scheduler/async';
import { ConnectionService } from './connection.service';
import { Toasty, ToastDuration, ToastPosition } from 'nativescript-toasty';
import { fromFile, ImageSource } from 'tns-core-modules/image-source/image-source';
import { LogGuardService } from './log.guard.service';
import { NavigationService } from './navigation.service';

@Injectable()
export class UploadService {


    bookingID;
    isUploadingPendingData: boolean;
    isInCheckScreen: boolean;
 // .then((res: any) => {
    //   if (!(res.access_token == "")) {
    //     let autherization_token: any;
    //     autherization_token = res.access_token;
    //     //   alert("Your access token is: " + accessToken);

    //     Values.writeString(Values.AUTHERIZATION_TOKEN, autherization_token);
    //     console.trace("Auth: ", autherization_token);
    //     // dialogs
    //     //   .confirm({
    //     //     title: "Your access token is: ",
    //     //     message: autherization_token,
    //     //     okButtonText: "OK"
    //     //   })
    //     //   .then(function (result) {
    //     //     if (result == true) {
    //     //       // alert("Login successfully");
    //     //       // this.routerExtensions.navigate(["/dashboard"]);
    //     //     }
    //     //   });
    //     this.routerExtensions.navigate(["/dashboard"]);
    //   } else {
    //     // alert(res.error);
    //     console.trace(res.error)
    //   }
    // }).catch((error) => {
    //   console.log(JSON.stringify(error) + "    dfghjkl;")
    // });
    // error => {
    //   alert(error);
    // }).catch((error) => {
    //   console.log(error)
    // });



    // this.http
    //   .post(Values.BASE_URL + "/api/login", this.user)
    //   .subscribe((res: any) => {
    //     if (!(res.access_token == "")) {
    //       let autherization_token: any;
    //       autherization_token = res.access_token;
    //       //   alert("Your access token is: " + accessToken);

    //       Values.writeString(Values.AUTHERIZATION_TOKEN, autherization_token);
    //       console.trace("Auth: ", autherization_token);
    //       // dialogs
    //       //   .confirm({
    //       //     title: "Your access token is: ",
    //       //     message: autherization_token,
    //       //     okButtonTex // .then((res: any) => {
    //   if (!(res.access_token == "")) {
    //     let autherization_token: any;
    //     autherization_token = res.access_token;
    //     //   alert("Your access token is: " + accessToken);

    //     Values.writeString(Values.AUTHERIZATION_TOKEN, autherization_token);
    //     console.trace("Auth: ", autherization_token);
    //     // dialogs
    //     //   .confirm({
    //     //     title: "Your access token is: ",
    //     //     message: autherization_token,
    //     //     okButtonText: "OK"
    //     //   })
    //     //   .then(function (result) {
    //     //     if (result == true) {
    //     //       // alert("Login successfully");
    //     //       // this.routerExtensions.navigate(["/dashboard"]);
    //     //     }
    //     //   });
    //     this.routerExtensions.navigate(["/dashboard"]);
    //   } else {
    //     // alert(res.error);
    //     console.trace(res.error)
    //   }
    // }).catch((error) => {
    //   console.log(JSON.stringify(error) + "    dfghjkl;")
    // });
    // error => {
    //   alert(error);
    // }).catch((error) => {
    //   console.log(error)
    // });



    // this.http
    //   .post(Values.BASE_URL + "/api/login", this.user)
    //   .subscribe((res: any) => {
    //     if (!(res.access_token == "")) {
    //       let autherization_token: any;
    //       autherization_token = res.access_token;
    //       //   alert("Your access token is: " + accessToken);

    //       Values.writeString(Values.AUTHERIZATION_TOKEN, autherization_token);
    //       console.trace("Auth: ", autherization_token);
    //       // dialogs
    //       //   .confirm({
    //       //     title: "Your access token is: ",
    //       //     message: autherization_token,
    //       //     okButtonText: "OK"
    //       //   })
    //       //   .then(function (result) {
    //       //     if (result == true) {
    //       //       // alert("Login successfully");
    //       //       // this.routerExtensions.navigate(["/dashboard"]);
    //       //     }
    //       //   });
    //       this.routerExtensions.navigate(["/dashboard"]);
    //     } else {
    //       // alert(res.error);
    //       console.trace(res.error)
    //     }
    //   },
    //     error => {
    //       alert(error);
    //     });t: "OK"
    //       //   })
    //       //   .then(function (result) {
    //       //     if (result == true) {
    //       //       // alert("Login successfully");
    //       //       // this.routerExtensions.navigate(["/dashboard"]);
    //       //     }
    //       //   });
    //       this.routerExtensions.navigate(["/dashboard"]);
    //     } else {
    //       // alert(res.error);
    //       console.trace(res.error)
    //     }
    //   },
    //     error => {
    //       alert(error);
    //     });
    constructor(private apiService: ApiService, private http: HttpClient, private connectionService: ConnectionService, private logGuardService: LogGuardService, private navigationService: NavigationService) {

        this.isUploadingPendingData = false;

        this.isInCheckScreen = false;

        setInterval(() => {

            var tempDamageString = Values.readString(Values.PENDING_DAMAGES, "");
            var tempSignatureString = Values.readString(Values.PENDING_CHECKS, "");

            if (tempDamageString != "" && tempDamageString != undefined && tempDamageString != "[]") {
                console.log("1", tempDamageString)
                if (this.connectionService.isConnected) {
                    if (!this.isUploadingPendingData) {
                        if (this.logGuardService.hasLoggedIn) {
                            if ((this.navigationService.getCurrentPage() != "Check") && (this.navigationService.getCurrentPage() != "Login"))
                                this.fun()
                        }
                    }
                }
            } else if (tempSignatureString != "" && tempSignatureString != undefined && tempSignatureString != "[]") {
                console.log("2", tempSignatureString)
                if (this.connectionService.isConnected) {
                    if (!this.isUploadingPendingData) {
                        if (this.logGuardService.hasLoggedIn) {
                            if ((this.navigationService.getCurrentPage() != "Check") && (this.navigationService.getCurrentPage() != "Login"))
                                this.fun()
                        }
                    }
                }
            } else {
                // console.log("No Pending Data")
            }
        }, 1000)
    }

    async fun() {

        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, ""),
        });

        var pendingDamageString = Values.readString(Values.PENDING_DAMAGES, "");
        var pendingSignatureString = Values.readString(Values.PENDING_CHECKS, "");

        if (this.apiService.isExternalAgentUploading != true) {
            if (pendingDamageString != "" && pendingDamageString != undefined) {

                var pendingDamages = [];
                var tempPendingDamages = [];

                pendingDamages = JSON.parse(Values.readString(Values.PENDING_DAMAGES, ""))

                for (var i = 0; i < pendingDamages.length; i++) {
                    tempPendingDamages.push(pendingDamages[i])
                }

                if (pendingDamages != null && pendingDamages != undefined) {

                    if (pendingDamages.length != undefined) {
                        for (var i = 0; i < pendingDamages.length; i++) {
                            if (pendingDamages[i] != undefined && pendingDamages[i] != null && pendingDamages[i].booking_id != "" && pendingDamages[i].booking_id != undefined) {
                                this.isUploadingPendingData = true;
                                pendingDamages[i].image_data = "data:image/jpeg;base64," + fromFile(pendingDamages[i].image_path).toBase64String("jpeg")

                                await this.apiService.postData(this.http, Values.BASE_URL + `/api/bookings/${pendingDamages[i].booking_id}/damages`, pendingDamages[i], { headers: headers })
                                    .then((res) => {

                                        console.log("Uploaded this Pending Damage:", res)
                                        success = true
                                        var index = tempPendingDamages.indexOf(pendingDamages[i])

                                        if (index > -1) {
                                            tempPendingDamages.splice(index, 1);
                                        }

                                        console.log("Pending Data Left", tempPendingDamages)


                                    }).catch((error) => {
                                        console.log("Error while uploading this Damage:", pendingDamages[i])
                                        success = false;

                                        if (error.message == 'Unauthenticated.' || error.message == 'Unauthenticated' || error.message == 'Unauthorized' || error.message == 'Unauthorized.') {
                                            alert('Session Expired, Please login again')
                                            setTimeout(() => {
                                                Values.writeString(Values.AUTHERIZATION_TOKEN, '');
                                                this.navigationService.navigateTo('login');
                                            }, 1000)
                                        }
                                    })
                            }
                            else {
                                var toasty = new Toasty("Booking Id not found", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
                            }
                        }
                    }
                }

                for (var i = 0; i < tempPendingDamages.length; i++) {
                    tempPendingDamages[i].image_data = "";
                }

                if (tempPendingDamages == [] || tempPendingDamages == undefined) {
                    Values.writeString(Values.PENDING_DAMAGES, "");
                }
                else {
                    Values.writeString(Values.PENDING_DAMAGES, JSON.stringify(tempPendingDamages));
                }
            }

            if (pendingSignatureString != "" && pendingSignatureString != undefined) {

                var pendingChecks = [];
                var tempPendingChecks = [];
                pendingChecks = JSON.parse(Values.readString(Values.PENDING_CHECKS, ""))

                for (var i = 0; i < pendingChecks.length; i++) {
                    tempPendingChecks.push(pendingChecks[i])
                }

                if (pendingChecks != null && pendingChecks != undefined) {

                    if (pendingChecks.length != undefined) {
                        for (var i = 0; i < pendingChecks.length; i++) {
                            if (pendingChecks[i] != undefined && pendingChecks[i] != null && pendingChecks[i].booking_id != "" && pendingChecks[i].booking_id != undefined) {
                                var success;
                                this.isUploadingPendingData = true;
                                var tempSigImgSrc: ImageSource;
                                if (pendingChecks[i].signature != "" && pendingChecks[i].signature != null) {
                                    tempSigImgSrc = fromFile(pendingChecks[i].signature)
                                }
                                else {
                                    var toasty = new Toasty("Could get Signatures Path", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
                                    return;
                                }

                                if (tempSigImgSrc == null && tempSigImgSrc == undefined) {
                                    var toasty = new Toasty("Image does not exist at specified Signature Path", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
                                    return;
                                }

                                pendingChecks[i].signature = "data:image/jpeg;base64," + tempSigImgSrc.toBase64String("jpeg");

                                await this.apiService.postData(this.http, Values.BASE_URL + `/api/bookings/${pendingChecks[i].booking_id}/${pendingChecks[i].checking_status}`, pendingChecks[i], { headers: headers })
                                    .then((res => {
                                        console.log("Uploaded this Pending Signature:", res)

                                        success = true
                                        var index = tempPendingChecks.indexOf(pendingChecks[i])

                                        if (index > -1) {
                                            tempPendingChecks.splice(index, 1);
                                        }

                                        console.log("Pending Signature Left:", tempPendingChecks)


                                    })).catch((error) => {
                                        console.log("Error while uploading this Signature:", pendingDamages[i])
                                        success = false;

                                        if (error.message == 'Unauthenticated.' || error.message == 'Unauthenticated' || error.message == 'Unauthorized' || error.message == 'Unauthorized.') {
                                            alert('Session Expired, Please login again')
                                            setTimeout(() => {
                                                Values.writeString(Values.AUTHERIZATION_TOKEN, '');
                                                this.navigationService.navigateTo('login');
                                            }, 1000)
                                        }
                                    })
                            }
                            else {
                                var toasty = new Toasty("Booking Id not found", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
                                continue;
                            }
                        }
                    }
                }

                for (var i = 0; i < tempPendingChecks.length; i++) {
                    tempPendingChecks[i].signature = "";
                }

                if (tempPendingChecks == [] || tempPendingChecks == undefined) {
                    Values.writeString(Values.PENDING_CHECKS, "");
                }
                else {
                    Values.writeString(Values.PENDING_CHECKS, JSON.stringify(tempPendingChecks));
                }

                console.log("Writting:", Values.readString(Values.PENDING_CHECKS, ""));

            }

            this.isUploadingPendingData = false;
        }
    }
}