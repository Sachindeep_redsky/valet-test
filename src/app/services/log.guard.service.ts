import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Values } from '../values/values';
import { Router } from '@angular/router';

@Injectable()
export class LogGuardService {

    private _logSubject;
    logChanges;
    public hasLoggedIn: boolean;


    constructor(private router: Router) {
        this._logSubject = new Subject<boolean>();
        this.logChanges = this._logSubject.asObservable();
        this.hasLoggedIn = false;
    }

    logIn() {
        this.hasLoggedIn = true;
    }

    logout() {
        // if (Values.readString(Values.AUTHERIZATION_TOKEN, "") != "") {
        Values.writeString(Values.AUTHERIZATION_TOKEN, "");
        // }
    }


}