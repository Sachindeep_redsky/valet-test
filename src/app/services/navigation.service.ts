import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { RouterExtensions } from 'nativescript-angular/router';
import { ExtendedNavigationExtras } from 'nativescript-angular/router/router-extensions';

@Injectable()
export class NavigationService {

    private _currentPageSubject;

    currentPageChanges;

    private currentPage: string;

    constructor(private routerExtensions: RouterExtensions) {
        this._currentPageSubject = new Subject<string>();

        this.currentPageChanges = this._currentPageSubject.asObservable();
        this.currentPage = "";
    }


    setCurrentPage(page: string) {
        this._currentPageSubject.next(page);
        this.currentPage = page;
    }

    getCurrentPage() {
        return this.currentPage;
    }

    navigateTo(pageName: string, extras?: ExtendedNavigationExtras) {
        if (extras) {
            this.routerExtensions.navigate(['/' + pageName], extras)
        } else {
            this.routerExtensions.navigate(['/' + pageName])
        }
    }

}