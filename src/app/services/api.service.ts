import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from "@angular/common/http"
import { HttpParams } from "@angular/common/http"
import { ConnectionService } from './connection.service';
import { Values } from '../values/values';
import { fromFile, ImageSource } from 'tns-core-modules/image-source/image-source';
import { File, Folder, knownFolders, path } from "tns-core-modules/file-system/file-system";
import { NavigationService } from './navigation.service';

@Injectable()
export class ApiService {

    private _canRequest: boolean;
    public isUploading: boolean;
    public isExternalAgentUploading: boolean;

    constructor(private connectionService: ConnectionService, private navigationService: NavigationService) {
        this.isUploading = false;
        this.isExternalAgentUploading = false;

        if (this.connectionService != undefined && this.connectionService.isConnected != undefined) {
            this._canRequest = this.connectionService.isConnected;
        }
    }

    public writeToDamageList(damageObj, bookingID: number) {
        var temp;
        var thisBookingFreeArray = [];
        var storageString = Values.readString(Values.PENDING_DAMAGES, "");
        if (storageString != "") {
            temp = JSON.parse(storageString);
            for (var i = 0; i < temp.length; i++) {
                if (temp.booking_id != bookingID) {
                    thisBookingFreeArray.push(temp[i]);
                }
            }
        }
        else {
            thisBookingFreeArray = [];
        }


        for (var i = 0; i < damageObj.length; i++) {
            damageObj[i].image_data = "";
            thisBookingFreeArray.push(damageObj[i])
        }

        if (thisBookingFreeArray == []) {
            Values.writeString(Values.PENDING_DAMAGES, "");
        }
        else {
            Values.writeString(Values.PENDING_DAMAGES, JSON.stringify(thisBookingFreeArray));
        }

        console.trace("Written_Dam", Values.readString(Values.PENDING_DAMAGES, ""))

    }


    async  postDamageObjects(http: HttpClient, damageObjects, bookingID: number) {

        this.isExternalAgentUploading = true;
        var tempDamages = [];

        for (var i = 0; i < damageObjects.length; i++) {
            tempDamages.push(damageObjects[i])
        }

        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, ""),
        });


        for (var i = 0; i < damageObjects.length; i++) {

            var imgSrc: ImageSource;
            imgSrc = fromFile(damageObjects[i].image_path);

            if (imgSrc == undefined || imgSrc == null) {
                console.log("Damage Image Source not found")
                continue;
            }

            damageObjects[i].image_data = "data:image/jpeg;base64," + imgSrc.toBase64String("jpeg")

            // return new Promise(async (resolve, reject) => { 

            await this.postData(http, Values.BASE_URL + `/api/bookings/${bookingID}/damages`, damageObjects[i], { headers: headers }).then((res) => {
                console.log("Uploaded this:", res)
                // resolve(res);
                var file: File;
                file = File.fromPath(damageObjects[i].image_path)

                if (file != undefined && file != null) {
                    file.remove().then((res) => {
                        console.log("File:", file, "Deleted")
                    }, error => {
                        console.log("Could not delete the file, Error:", error)
                    });
                }

                var index = tempDamages.indexOf(damageObjects[i])

                if (index > -1) {
                    tempDamages.splice(index, 1);
                }
            }, error => {
                // reject(error)
                if (error.message == 'Unauthenticated.' || error.message == 'Unauthenticated' || error.message == 'Unauthorized' || error.message == 'Unauthorized.') {
                    alert('Session Expired, Please login again')
                    setTimeout(() => {
                        Values.writeString(Values.AUTHERIZATION_TOKEN, '');
                        this.navigationService.navigateTo('login');
                    }, 1000)
                }
                console.log("Error while uploading the Damages:", error);
            })
            // })


        }
        this.writeToDamageList(tempDamages, bookingID);
        this.isExternalAgentUploading = false;
    }

    async getData(http: HttpClient, url: string, options?: {
        headers?: HttpHeaders | {
            [header: string]: string | string[];
        };
        observe?: 'body';
        params?: HttpParams | {
            [param: string]: string | string[];
        };
        reportProgress?: boolean;
        responseType?: 'json';
        withCredentials?: boolean;
    }): Promise<Object> {

        var that = this;

        var promise = new Promise<Object>(async function (resolve, errorp) {
            if (that._canRequest) {
                that.isUploading = true;
                await http.get(url, options).subscribe(res => {
                    resolve(res);
                    setTimeout(() => {
                    }, 1000)
                },
                    error => {
                        errorp(error)
                        setTimeout(() => {
                        }, 1000)
                    })
            } else {
                errorp("Internet not working")
                setInterval(() => {
                }, 100)
            }
        }) 

        return promise;
    }

    postData(http: HttpClient, url: string, body: any | null, options?: {
        headers?: HttpHeaders | {
            [header: string]: string | string[];
        };
        observe?: 'body';
        params?: HttpParams | {
            [param: string]: string | string[];
        };
        reportProgress?: boolean;
        responseType?: 'json';
        withCredentials?: boolean;
    }): Promise<Object> {
        var that = this;
        var promise = new Promise<Object>(async function (resolve, errorp) {


            if (!that._canRequest) {
                that.isUploading = true;
                await http.post(url, body, options).subscribe(res => {
                    resolve(res);
                    setTimeout(() => {
                        that.isUploading = false;
                    }, 1000)
                },
                    error => {
                        errorp(error)
                        setTimeout(() => {
                            that.isUploading = false;
                        }, 1000)
                    })
            }
            else {
                errorp("Internet not working")
                setInterval(() => {
                }, 100)
            }
        })

        return promise;
    }

    putData(http: HttpClient, url: string, body: any | null, options?: {
        headers?: HttpHeaders | {
            [header: string]: string | string[];
        };
        observe?: 'body';
        params?: HttpParams | {
            [param: string]: string | string[];
        };
        reportProgress?: boolean;
        responseType?: 'json';
        withCredentials?: boolean;
    }): Promise<Object> {

        var that = this;
        var promise = new Promise<Object>(async function (resolve, errorp) {
            if (that._canRequest) {
                that.isUploading = true;

                await http.put(url, body, options).subscribe(res => {
                    resolve(res)
                    setTimeout(() => {
                        that.isUploading = false;
                    }, 1000)
                },
                    error => {
                        errorp(error)
                        setTimeout(() => {
                            that.isUploading = false;
                        }, 1000)
                    })
            } else {
                errorp("Internet not working")
                setInterval(() => {
                }, 100)
            }
        })
        return promise;
    }

}