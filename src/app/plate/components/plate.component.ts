import { Component, OnInit, OnDestroy, ViewChild, ElementRef } from "@angular/core";
import { setCurrentOrientation } from "nativescript-screen-orientation";
import { ObservableArray } from 'tns-core-modules/data/observable-array/observable-array';

@Component({
    selector: "app-plate",
    moduleId: module.id,
    templateUrl: "./plate.component.html",
    styleUrls: ["./plate.component.css"]
})

export class PlateComponent implements OnInit, OnDestroy {
    @ViewChild('scanningline', { static: false }) scl: ElementRef;

    listItems;

    constructor() {
        setCurrentOrientation("landscape", function () {
            console.log("set landscape orientation");
        });

        this.listItems = new ObservableArray();

        this.listItems.push({ "Ludo": "4455" })
        this.listItems.push({ "Ludo": "4455" })
        this.listItems.push({ "Ludo": "4455" })
        this.listItems.push({ "Ludo": "4455" })
        this.listItems.push({ "Ludo": "4455" })
        this.listItems.push({ "Ludo": "4455" })
        this.listItems.push({ "Ludo": "4455" })
        this.listItems.push({ "Ludo": "4455" })
        this.listItems.push({ "Ludo": "4455" })
        this.listItems.push({ "Ludo": "4455" })
        this.listItems.push({ "Ludo": "4455" })
        this.listItems.push({ "Ludo": "4455" })
    }

    public onTextRecognitionResult(args) {
        console.log(args)
    }

    ngOnInit(): void {
    }

    ngOnDestroy(): void {
    }

    onBackPress(any) {

    }
}