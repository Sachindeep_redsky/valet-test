import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { PlateComponent } from "./components/plate.component";
import { PlateRoutingModule } from "./plate-routing.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        PlateRoutingModule
    ],
    declarations: [PlateComponent],

    schemas: [NO_ERRORS_SCHEMA]
})
export class PlateModule { }
