import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { HttpClientModule } from "@angular/common/http";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { GestureService } from "./gestures/gesture.services";
import { registerElement } from "nativescript-angular/element-registry";
import { ConnectionService } from './services/connection.service';
import { NativeScriptFormsModule } from 'nativescript-angular/forms';
import { NativeScriptHttpClientModule } from 'nativescript-angular/http-client';
import { NativeScriptHttpModule } from 'nativescript-angular/http';
import { ApiService } from './services/api.service';
import { UploadService } from './services/upload.service';
import { LogGuardService } from "./services/log.guard.service";
import { NavigationService } from "./services/navigation.service";
import { NativeScriptUISideDrawerModule } from "nativescript-ui-sidedrawer/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular"
// import { HeaderListComponent } from "./headerList/header-list.component"

registerElement("MLKitTextRecognition", () => require("nativescript-plugin-firebase/mlkit/textrecognition").MLKitTextRecognition);

@NgModule({
  bootstrap: [AppComponent],
  imports: [
    NativeScriptModule,
    AppRoutingModule,
    HttpClientModule,
    NativeScriptHttpModule,
    NativeScriptHttpClientModule,
    NativeScriptCommonModule,
    NativeScriptUISideDrawerModule,
    NativeScriptUIListViewModule,
    NativeScriptFormsModule
  ],
  declarations: [AppComponent],
  providers: [GestureService, ConnectionService, ApiService, UploadService, LogGuardService, NavigationService],
  schemas: [NO_ERRORS_SCHEMA]
})

export class AppModule { }
