import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { CheckRoutingModule } from "./check-routing.module";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { CheckComponent } from "./components/check.component";
import { SwitchButtonModule } from "../switch-module/switch.module";
import { NgModalModule } from '../modals/ng-modal';
import { HeaderListComponent } from '../headerList/header-list.component'


@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        NativeScriptHttpClientModule,
        CheckRoutingModule,
        NgModalModule,
        SwitchButtonModule
    ],
    declarations: [
        CheckComponent,
        HeaderListComponent
    ],
    schemas: [NO_ERRORS_SCHEMA]
})

export class CheckModule { }
