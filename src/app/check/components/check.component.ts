import { Component, ViewChild, ElementRef, OnInit, AfterContentInit, OnDestroy, AfterViewInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { fromNativeSource, ImageSource, fromAsset, fromFile } from "image-source";
import { registerElement } from "nativescript-angular/element-registry";
import { ActivatedRoute } from "@angular/router";
import { File, Folder, knownFolders, path } from "tns-core-modules/file-system/file-system";
import { Values } from "~/app/values/values";
import { Image } from "ui/image"
import { Page, EventData, isAndroid, isIOS, Observable } from "tns-core-modules/ui/page/page";
import { Menu } from "nativescript-menu";
import { TextField } from "tns-core-modules/ui/text-field";
import { Damage } from "~/app/models/damage.model";
import { Button } from 'ui/button/button'
import { GestureService } from "~/app/gestures/gesture.services";
import { ImageAsset } from 'tns-core-modules/image-asset/image-asset';
import { ConnectionService } from '~/app/services/connection.service';
import { CheckInOutPost } from "../../models/check-in-out-post.model"
import { StackLayout } from 'tns-core-modules/ui/layouts/stack-layout/stack-layout';
import { RouterExtensions } from 'nativescript-angular/router/router-extensions'
import { ModalComponent } from "~/app/modals/modal.component";
import { Toasty, ToastDuration, ToastPosition } from 'nativescript-toasty';
import { ObservableArray } from "tns-core-modules/data/observable-array";
import { ApiService } from "~/app/services/api.service";
import { NavigationService } from "~/app/services/navigation.service";

import * as imagepicker from "nativescript-imagepicker";
import * as camera from "nativescript-camera";
import * as application from "tns-core-modules/application";
import * as Permissions from "nativescript-permissions";
import * as moment from "moment";

registerElement("DrawingPad", () => require("nativescript-drawingpad").DrawingPad);

declare var android: any;
declare var PHPhotoLibrary, PHAuthorizationStatus: any


class UpgradeListItem {

    name: string;
    amount: number;
    code: string;
    items: any[];

    constructor() {
        this.name = '';
        this.amount = 0;
        this.code = '';
        this.items = [];
    }
}


@Component({
    selector: "check",
    moduleId: module.id,
    templateUrl: "./check.component.html",
    styleUrls: ["./check.component.css"]
})

export class CheckComponent implements OnInit, AfterViewInit, OnDestroy {

    @ViewChild('selectionDialog', { static: false }) selectionModal: ModalComponent;
    @ViewChild('damageDialog', { static: false }) damageModal: ModalComponent;
    @ViewChild('damageImageConfirm', { static: false }) damageImageConfirm: ModalComponent;
    @ViewChild('welcomeDialog', { static: false }) welcomeModal: ModalComponent;
    @ViewChild('upgradeList', { static: false }) upgradeListModel: ModalComponent;

    @ViewChild("signaturePad", { static: false }) public drawingPad: ElementRef;
    @ViewChild("image", { static: false }) public imageRef: ElementRef;
    @ViewChild("damageList", { static: false }) public damageListRef: ElementRef;
    @ViewChild("button", { static: false }) public buttonRef: ElementRef;

    details;
    img: Image;

    viewModel;
    imageAssets;
    damageImageSource: string;
    list;
    actions;
    damageImageSrcs;
    damageItems;
    optedOptionalServices;
    availableOptionalServices;
    upgradableListItems;

    signatureSource: ImageSource;
    signatureSourcePath: string;

    private pad: any;

    bookingModel;

    isGetImageEnabled: boolean;
    isUploadEnabled: boolean;
    isCarDamageSelected: boolean;
    isSignatureSelected: boolean;
    isOtherServicesSelected: boolean;
    isDrawing: boolean;
    canCheck: boolean;
    isBusy: boolean;
    isSingleMode: boolean;
    isUpdatingDamage: boolean;
    isUpdatingImage: boolean;

    postionType: string;
    positionText: string;
    recordedOn: string;
    checkButtonText: string;
    imageBaseString: string;
    padBaseString: string;
    bookingID: number;
    currentDamageId: number;
    tempSignatureImageName: string;
    checkInOutButtonColor: string;
    baseUrls: string;
    pageRef: Page;

    button: Button;

    renderViewTimeout;
    isEmpty: boolean;


    // <---------------------new Design ----------------------------------->

    appLogo: string;
    appTheme: any;
    redColor: string;
    blueColor: string;
    grayColor: string;
    whiteColor: string;
    colors: any[];
    showCarsDetailsCard: boolean;
    showSignatureCard: boolean;
    showAddDamageCard: boolean;
    showServicesCard: boolean;
    showDamageAndsignatureCard: boolean;
    captureButtonShow: boolean;
    promoCode: string;
    userName: string;
    email: string;
    mobile: string;
    CarModel: string;
    carPlate: string;
    carColor: string;
    carKm: string;
    parkingSpace: string;
    flightNo: string;
    arrivalDate: string;
    arriveTime: string;
    depatureDate: string;
    DepartureTime: string;
    selectedColorIndex: number;
    currentDamageImage: string;
    isRendering: boolean;

    // <---------------------new Design ----------------------------------->

    constructor(private activatedRoute: ActivatedRoute, private routerExtensions: RouterExtensions, private http: HttpClient, private connectionService: ConnectionService, private apiService: ApiService, private gestureService: GestureService, private navigationService: NavigationService) {

        //    <--------------------------------newDesign----------------------------------->
        this.refreshTheme()
        this.colors = ["000000", "c0c0c0", "ff0000", "87ceeb", "008000", "e9c204", "ffffff", "0000ff", "808080"];
        this.showCarsDetailsCard = false;
        this.showSignatureCard = false;
        this.showAddDamageCard = false;
        this.showServicesCard = false;
        this.showDamageAndsignatureCard = false;
        this.captureButtonShow = true;
        this.currentDamageImage = "";
        this.isRendering = false;
        //    <--------------------------------newDesign----------------------------------->

        this.activatedRoute.queryParams.subscribe(params => {
            this.bookingModel = JSON.parse(params["booking"])
            console.trace("Parsss Data :::", this.bookingModel)
        });

        this.optedOptionalServices = [];
        this.upgradableListItems = [];
        this.navigationService.setCurrentPage("Check")
        this.actions = [];
        this.actions.push("Add Car Damage Images");
        this.actions.push("Add Signature Images");
        this.damageImageSrcs = [];
        this.list = new ObservableArray();
        this.baseUrls = Values.BASE_URL;
        this.imageAssets = [];
        this.imageBaseString = "";
        this.padBaseString = "";
        this.tempSignatureImageName = "";

        this.isDrawing = true;
        this.isBusy = false;
        this.isSingleMode = true;
        this.bookingID = 0;
        this.currentDamageId = null;
        this.userName = '';
        this.carPlate = '';
        this.checkButtonText = "";

        this.postionType = "0";
        this.positionText = "";
        this.recordedOn = "";

        this.isRendering = true;

        this.isGetImageEnabled = true;
        this.isUploadEnabled = true;

        this.isCarDamageSelected = false;
        this.isSignatureSelected = true;
        this.isOtherServicesSelected = false;
        this.isRendering = false;

        this.canCheck = false;
        this.isUpdatingDamage = false;
        this.isUpdatingImage = false;

        this.availableOptionalServices = this.getAvailableOptionalServices();

        if (this.bookingModel != undefined) {
            if (this.bookingModel.userInfo.id) {
                this.promoCode = this.bookingModel.userInfo.id;
            }
            if (this.bookingModel.userInfo.name) {
                this.userName = this.bookingModel.userInfo.name;
            }
            if (this.bookingModel.userInfo.email) {
                this.email = this.bookingModel.userInfo.email;
            }
            if (this.bookingModel.userInfo.mobile_no) {
                this.mobile = this.bookingModel.userInfo.mobile_no;
            }
            if (this.bookingModel.carInfo.model) {
                this.CarModel = this.bookingModel.carInfo.model;
            }
            if (this.bookingModel.carInfo.license_plate) {
                this.carPlate = this.bookingModel.carInfo.license_plate;
            }
            if (this.bookingModel.carInfo.carKm) {
                this.carKm = this.bookingModel.carInfo.license_plate;
            }
            if (this.bookingModel.optionalServices && this.bookingModel.optionalServices.length != 0) {
                this.parkingSpace = this.bookingModel.optionalServices[0].name.en;
            }
            if (this.bookingModel.departure.flight_number) {
                this.flightNo = this.bookingModel.departure.flight_number;
            }
            if (this.bookingModel.departure.departure_at) {
                this.depatureDate = moment(this.bookingModel.departure.departure_at).format('DD MMM YYYY');
                this.DepartureTime = moment(this.bookingModel.departure.departure_at).format('HH: mm');
            }
            if (this.bookingModel.arrival.arrival_at) {
                this.arrivalDate = moment(this.bookingModel.arrival.arrival_at).format('DD MMM YYYY');
                this.arriveTime = moment(this.bookingModel.arrival.arrival_at).format('HH: mm');
            }

            if (this.bookingModel.id != undefined && this.bookingModel.id != 0 && this.bookingModel.id != null) {
                this.bookingID = this.bookingModel.id;
            }

            if (this.bookingModel.optionalServices) {
                this.optedOptionalServices = this.bookingModel.optionalServices
            }


            //Uncomment it

            if (this.bookingModel.checkin != null && this.bookingModel.checkout != null) {
                this.recordedOn = "";
                this.checkButtonText = "Complete"
                this.canCheck = false;
                this.checkInOutButtonColor = "gray"
            } else if (this.bookingModel.checkin != null && this.bookingModel.checkout == null) {
                this.recordedOn = "checkout";
                this.checkButtonText = "Check-Out"
                this.checkInOutButtonColor = "#3498db"
            }
            else if (this.bookingModel.checkin == null && this.bookingModel.checkout == null) {
                this.recordedOn = "checkin";
                this.checkButtonText = "Check-In"
                this.checkInOutButtonColor = "#3498db"
            }
            else {
                this.recordedOn = "";
                this.checkButtonText = "Complete"
                this.canCheck = false;
                this.checkInOutButtonColor = "gray"
            }
        }

        //Comment it 

        // this.recordedOn = "checkout";
        // this.checkButtonText = "Check-Out"
        // this.checkInOutButtonColor = "#3498db"

        if (isAndroid) {
            application.android.on(application.AndroidApplication.activityBackPressedEvent, (args: any) => {
                args.cancel = true;
                this.routerExtensions.back();
            });

            Permissions.requestPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, "Needed for creating Files").then(() => {
                console.log("Permission granted!");
            }).catch(() => {
                console.log("Permission is not granted");
            });

            Permissions.requestPermission(android.Manifest.permission.READ_EXTERNAL_STORAGE, "Needed for getting Files").then(() => {
                console.log("Permission granted!");
            }).catch(() => {
                console.log("Permission is not granted");
            });
        }

        this.optionalServicesUpgradeChecker();

    }

    public ngOnInit() {
        this.showCarsDetailsCard = true;
        if (isAndroid) {
            application.android.on(application.AndroidApplication.activityBackPressedEvent, (data: application.AndroidActivityBackPressedEventData) => {
                data.cancel = false; // prevents default back button behavior
                this.routerExtensions.back();
            });
        }

        this.getDamageListFromServer();
        // this.getDamagesListFromStorage();
    }

    public ngAfterViewInit() {

        setTimeout(() => {
            // this.pad = this.drawingPad.nativeElement;
            // this.button = this.buttonRef.nativeElement as Button;
        }, 5)

        // this.gestureService.setGestureEvent(this.button, 'longPress');
    }


    public onSignaturePadLoaded(args: any) {
        this.pad = args.object;
    }

    getAvailableOptionalServices() {

        var availabeOtionalServicesStr = Values.readString(Values.OPTIONAL_SERVICES, '');
        var availabeOtionalServices = [];

        if (availabeOtionalServicesStr) {
            availabeOtionalServices = JSON.parse(availabeOtionalServicesStr);
        }

        return availabeOtionalServices;
    }


    optionalServicesUpgradeChecker() {

        var upgradableOptionalServices = [];

        for (var i = 0; i < this.availableOptionalServices.length; i++) {

            for (var j = 0; j < this.optedOptionalServices.length; j++) {

                if (this.availableOptionalServices[i].code == this.optedOptionalServices[j].code) {

                    console.log('Matched:::', this.availableOptionalServices[i].code)

                    if (this.availableOptionalServices[i].service_group && this.availableOptionalServices[i].service_group.name && this.availableOptionalServices[i].service_group.name.en) {
                        var serviceGrp = this.availableOptionalServices[i].service_group.name.en;
                        var rateAmount = this.availableOptionalServices[i].rate.amount;

                        for (var k = 0; k < this.availableOptionalServices.length; k++) {

                            if (this.availableOptionalServices && this.availableOptionalServices[k] && this.availableOptionalServices[k].service_group && this.availableOptionalServices[k].service_group.name && this.availableOptionalServices[k].service_group.name.en == serviceGrp) {
                                console.log('Matched:::GRP::', serviceGrp)

                                if (this.availableOptionalServices && this.availableOptionalServices[k] && this.availableOptionalServices[k].rate && this.availableOptionalServices[k].rate.amount > rateAmount) {
                                    console.log('Matched:::More::price:::', this.availableOptionalServices[k].rate.amount)

                                    if (upgradableOptionalServices && upgradableOptionalServices.length > 0) {
                                        for (var t = 0; t < upgradableOptionalServices.length; t++) {

                                            if (upgradableOptionalServices[t].optedOptionalService == this.optedOptionalServices[j]) {
                                                upgradableOptionalServices[t].upgradableTo.push(this.availableOptionalServices[k])
                                            } else {
                                                upgradableOptionalServices.push({
                                                    'optedOptionalService': this.optedOptionalServices[j],
                                                    'upgradableTo': [this.availableOptionalServices[k]]
                                                })
                                            }
                                        }
                                    } else {
                                        upgradableOptionalServices.push({
                                            'optedOptionalService': this.optedOptionalServices[j],
                                            'upgradableTo': [this.availableOptionalServices[k]]
                                        })
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        console.trace('Upgradables::', upgradableOptionalServices)

        this.prepareUpgradableItems(upgradableOptionalServices)

        // merging upgrades for a single optional service
        // var upgradableOptionalServicesCommon = [];
        // for (var z = 0; z < upgradableOptionalServices.length; z++) {
        //     upgradableOptionalServicesCommon.push({
        //         'optedOptionalService': upgradableOptionalServices[z],

        //     })
        // }

        // for (var p = 0; p < upgradableOptionalServices.length; p++) {
        //     for (var q = 0; q < upgradableOptionalServices.length; q++) {
        //         if (upgradableOptionalServices[p].optedOptionalService == upgradableOptionalServices[q].optedOptionalService) {

        //         }
        //     }
        // }

    }

    prepareUpgradableItems(upgradableOptionalServices) {
        this.upgradableListItems = [{ "name": '', "amount": 0, "code": '', "items": [] }];
        // let item = { "name": '', "amount": 0, "code": '', "items": [] };

        // if (upgradableOptionalServices && upgradableOptionalServices.length > 0) {
        //     for (var i = 0; i < upgradableOptionalServices.length; i++) {

        //         item["name"] = upgradableOptionalServices[i].optedOptionalService.name
        //         item["amount"] = upgradableOptionalServices[i].optedOptionalService.amount
        //         item["code"] = upgradableOptionalServices[i].optedOptionalService.code

        //         for (var j = 0; j < upgradableOptionalServices[i].upgradableTo.length; j++) {
        //             item["items"].push({
        //                 name: upgradableOptionalServices[i].upgradableTo[j].name,
        //                 code: upgradableOptionalServices[i].upgradableTo[j].code,
        //                 amount: upgradableOptionalServices[i].upgradableTo[j].rate.amount
        //             })
        //         }

        //         if (item && item.name && item.amount && item.code && item.items) {
        //             this.upgradableListItems.push(item);
        //             console.log("itemssssssssssssssssss", this.upgradableListItems)
        //         }
        //     }
        // }
        // this.upgradableListItems && this.upgradableListItems.length != 0 ? this.isEmpty = false : this.isEmpty = true
        // console.log('List::', this.upgradableListItems)
    }


    // getOptionalServices() {

    //     let headers = new HttpHeaders({
    //         "Content-Type": "application/json",
    //         "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, "")
    //     });

    //     this.http.get(Values.BASE_URL + `/api/bookings/${booking}/optional-services/${optional_service}`, { headers: headers }).subscribe((res: any) => {
    //         console.trace('osss:::', res)
    //     }, error => {
    //         console.trace('error:::', error)
    //     })

    // }

    onPageLoaded(args: EventData) {
        this.pageRef = args.object as Page;
        var menu = this.pageRef.getViewById("menuButton") as StackLayout;
    }

    public openMenu() {

        if (this.recordedOn == "") {
            return
        }

        Menu.popup({
            view: this.pageRef.getViewById("menuButton"),
            actions: this.actions
        }).then((value: any) => {
            console.log("Value:", value)
            var tempValue;

            console.log("Type:", typeof (value))

            if (typeof (value) == 'string') {
                tempValue = value;
            } else if (typeof (value) == 'object') {
                if (value != undefined && value.title != undefined) {
                    tempValue = value.title;
                }
            }

            if (tempValue == this.actions[0]) {
                this.isCarDamageSelected = true;
                this.isSignatureSelected = false;
                this.isOtherServicesSelected = false;
                this.selectionModal.show();
            }
            else if (tempValue == this.actions[1]) {
                this.isCarDamageSelected = false;
                this.isSignatureSelected = true;
                this.isOtherServicesSelected = false;
                this.selectionModal.show();
            }
            else if (tempValue == this.actions[2]) {
                this.isCarDamageSelected = false;
                this.isSignatureSelected = false;
                this.isOtherServicesSelected = true;
                // this.isUploadEnabled = false;
            }
            this.isGetImageEnabled = false;
        });
    }

    getDamageListFromServer() {

        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, "")
        });
        // this.isRendering = true;

        this.http.get(Values.BASE_URL + `/api/bookings/${this.bookingID}/damages`, { headers: headers }).subscribe((res: any) => {
            console.trace('DamageList:::', res)
            this.damageItems = [];

            for (var i = 0; i < res.data.length; i++) {
                this.damageItems.push(res.data[i]);
            }

            this.viewModel = new Observable();
            this.viewModel.set("items", this.damageItems);

            setTimeout(() => {
                // this.isRendering = false;
            }, 400)

        }, error => {
            setTimeout(() => {
                // this.isRendering = false;
            }, 400)
            if (error.error.message == 'Unauthenticated.') {
                alert('Session Expired, Please login again')
                setTimeout(() => {
                    Values.writeString(Values.AUTHERIZATION_TOKEN, '');
                    this.routerExtensions.navigate(['/login'])
                }, 400)
            }
        })
    }

    createDamageForBooking(damageModel) {

        console.log('Damage Model:::', damageModel)

        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Accept": "application/json",
            "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, "")
        });

        this.isRendering = true;
        console.log('Booking ID:::', this.bookingID)

        this.http.post(Values.BASE_URL + `/api/bookings/${this.bookingID}/damages`, damageModel, { headers: headers }).subscribe((res: any) => {
            console.trace(res)

            setTimeout(() => {
                this.getDamageListFromServer();
                this.isRendering = false;
                this.captureButtonShow = true;0
                var toasty = new Toasty("Damage Create Successfully.", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
            }, 10)

        }, error => {
            console.trace('Damage:::Create:::Error:::', error)
            alert('Damage:::Create:::Error:::' + JSON.stringify(error))

            setTimeout(() => {
                this.isRendering = false;
            }, 10)

            if (error.error.message == 'Unauthenticated.') {
                alert('Session Expired, Please login again')
                setTimeout(() => {
                    Values.writeString(Values.AUTHERIZATION_TOKEN, '');
                    this.routerExtensions.navigate(['/login'])
                }, 400)
            }
        })
    }

    updateDamage(damageModel) {

        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, "")
        });

        this.isRendering = true;

        this.http.put(Values.BASE_URL + `/api/bookings/${this.bookingID}/damages/${this.currentDamageId}`, damageModel, { headers: headers }).subscribe((res: any) => {
            console.trace(res)

            setTimeout(() => {
                this.getDamageListFromServer();
                this.isRendering = false;
            }, 10)

        }, error => {
            console.trace('Damage:::Update:::Error:::', error)

            setTimeout(() => {
                this.isRendering = false;
            }, 10)

            if (error.error.message == 'Unauthenticated.') {
                alert('Session Expired, Please login again')
                setTimeout(() => {
                    Values.writeString(Values.AUTHERIZATION_TOKEN, '');
                    this.routerExtensions.navigate(['/login'])
                }, 400)
            }
        })
    }

    //Damage Related Operations
    public getDamagesListFromStorage() {

        var temp = [];
        var storageString = Values.readString(Values.PENDING_DAMAGES, "");

        if (storageString != "") {
            var arr = [];
            arr = JSON.parse(storageString);
            for (var i = 0; i < arr.length; i++) {
                if (arr[i].booking_id == this.bookingID) {
                    temp.push(new Damage(arr[i]))
                }
            }
        }
        else {
            temp = [];
        }

        this.damageImageSrcs = temp;
    }

    public onTextChangePositionType(args) {
        var positionTextField = <TextField>args.object;
        this.positionText = positionTextField.text;
    }

    onLeftClick() {
        this.postionType = "inside";
        console.log("Inside", this.postionType)
    }

    onRightClick() {
        this.postionType = "outside";
    }

    public writeToDamageList(damageObj) {

        var temp = [];
        var storageString = Values.readString(Values.PENDING_DAMAGES, "");

        if (storageString != "") {
            var arr = [];
            arr = JSON.parse(storageString);
            for (var i = 0; i < arr.length; i++) {
                temp.push(new Damage(arr[i]))
            }
        }
        else {
            temp = [];
        }
        temp.push(damageObj);
        Values.writeString(Values.PENDING_DAMAGES, JSON.stringify(temp))
        this.getDamagesListFromStorage();
    }

    public onDamageSubmit() {

        var that = this;
        var damageModel = new Damage();

        //just check, can remove
        damageModel.id = this.currentDamageId;
        damageModel.position_type = this.postionType;
        damageModel.position = this.positionText;
        damageModel.recorded_on = this.recordedOn;
        damageModel.booking_id = this.bookingID;
        damageModel.image_path = this.damageImageSource;

        console.log('Damamaage Model:::', damageModel)

        if (this.positionText != '' && this.postionType != '0') {

            if (this.damageImageSource != null && this.damageImageSource != "") {

                damageModel.id = this.currentDamageId;
                damageModel.position_type = this.postionType;
                damageModel.position = this.positionText;
                damageModel.recorded_on = this.recordedOn;
                damageModel.booking_id = this.bookingID;
                damageModel.image_path = this.damageImageSource;

                if (this.isUpdatingDamage) {
                    if (this.isUpdatingImage) {
                        damageModel.image_data = 'data:image/png;base64,' + fromFile(this.damageImageSource).toBase64String('png');

                    } else {
                        damageModel.image_data = undefined;
                    }

                    this.updateDamage(damageModel);
                } else {
                    damageModel.id = null;
                    damageModel.image_data = 'data:image/png;base64,' + fromFile(this.damageImageSource).toBase64String('png');
                    let preciseModel = {};
                    (preciseModel as any).recorded_on = damageModel.recorded_on;
                    (preciseModel as any).booking_id = damageModel.booking_id;
                    (preciseModel as any).position = damageModel.position;
                    (preciseModel as any).position_type = damageModel.position_type;
                    (preciseModel as any).image_data = damageModel.image_data;
                    this.createDamageForBooking(preciseModel);
                }

                // this.damageModal.hide();
            }
            else {
                var toasty = new Toasty("Could not get image file", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
                damageModel = new Damage();
                console.log("Could not get image file")
                this.damageModal.hide();
                return;
            }
        }
        else {
            console.log("Position can not be empty")
            var toasty = new Toasty("Position can not be empty", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
            damageModel = new Damage();
            return;
        }
    }

    onItemTap(item: any, i) {
        // this.isUpdatingDamage = true;
        // this.currentDamageId = this.damageItems[item.index].id;
        // this.damageImageConfirm.show();
        this.currentDamageImage = item.filename;
    }

    onImageUpdateYes() {
        this.isUpdatingImage = true;
        this.isCarDamageSelected = true;
        this.isSignatureSelected = false;
        this.isOtherServicesSelected = false;
        console.log("Value:", "car")
        this.selectionModal.show();
    }

    onImageUpdateNo() {
        this.isUpdatingImage = false;
        this.damageModal.show();
    }

    pendingDamagesChecker = async () => {

        var folder

        if (isAndroid) {
            folder = Folder.fromPath("/storage/emulated/0/Valet/Pending/Damages");

        }
        else if (isIOS) {
            folder = knownFolders.documents();
        }

        var that = this;
        var noOfBookingDamages: number;
        var indexArray = [];

        await folder.getEntities().then((res) => {

            if (res != undefined && res != null && res.length != 0) {
                for (var i = 0; i < res.length; i++) {
                    var fileName = res[i].name;

                    if (fileName.search(that.bookingID.toString()) != -1) {
                        var file = File.fromPath(res[i].path)
                        var len = that.bookingID.toString().length;

                        var indexWithExtension = fileName.substr(len);
                        var index = Number(indexWithExtension.replace(file.extension, ''))

                        if (index != NaN) {
                            indexArray.push(index)
                        }
                        else {
                        }
                    }
                    else {
                    }
                }
            }
        },
            error => {
                console.log("Error:", error)
            })

        if (indexArray != undefined && indexArray.length != 0) {
            indexArray.sort(function (a, b) { return a - b });
            noOfBookingDamages = indexArray[indexArray.length - 1];
        }
        return noOfBookingDamages;
    }

    //Signature Related
    public saveSignature() {
        this.drawingPadImageGetter();
    }

    public updateImage(imageRes) {
        var img = this.imageRef.nativeElement as Image;
        img.src = imageRes;

    }

    drawingPadImageGetter = async () => {
        let that = this;
        // this.pad = that.drawingPad.nativeElement;

        await this.pad.getDrawing().then(data => {
            console.log("Res:", data);

            var fileName = "PS_" + that.bookingID + ".jpeg"

            if (isAndroid) {
                that.signatureSourcePath = "/storage/emulated/0/Valet/Pending/Signatures/" + fileName;

            } else if (isIOS) {
                that.signatureSourcePath = path.join(knownFolders.documents().path, fileName);
            }

            var file: File;
            file = File.fromPath(that.signatureSourcePath);

            var signatureSourceTemp = fromNativeSource(data)
            signatureSourceTemp.saveToFile(that.signatureSourcePath, "jpeg");

            // for Image path testing
            var img = new ImageSource();
            img = fromFile(that.signatureSourcePath);
            that.signatureSource = img;
            that.canCheck = true;
            var toasty = new Toasty("Signature Accepted", ToastDuration.SHORT, ToastPosition.BOTTOM).show();


        }, error => {
            that.canCheck = false;
            console.log(that.pad);
            console.log("Error:", error);
            var toasty = new Toasty("Please signature on Pad!", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
        });
    }

    //Image Related
    public captureImage() {

        let that = this;
        camera.requestCameraPermissions().then(() => {
        },
            error => {
                // var toasty = new Toasty("Camera permission denied", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
                console.log("Camera permission denied");
            }
        )

        camera.requestPhotosPermissions().then(() => {
        },
            error => {
                // var toasty = new Toasty("Photos permission denied", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
                console.log("Photos permission denied", error);

            }
        )

        camera.takePicture({ width: 128, height: 128, keepAspectRatio: true, cameraFacing: "rear" }).then((imgAssest: ImageAsset) => {
            setTimeout(() => {
                that.captureButtonShow = false;
            }, 50);

            // if (that.isSignatureSelected) {

            //     if (isAndroid) {

            //         var removableFile = File.fromPath(imgAssest.android);
            //         var file = File.fromPath("/storage/emulated/0/Valet/Pending/Signatures/" + "PS_" + that.bookingID + removableFile.extension);

            //         fromAsset(imgAssest).then(imageSource => {
            //             imageSource.saveToFile(file.path, "jpeg");
            //             that.updateImage(imageSource);
            //         });

            //         that.signatureSourcePath = file.path;
            //     }
            //     else if (isIOS) {

            //         fromAsset(imgAssest).then((imageSource) => {
            //             PHPhotoLibrary.requestAuthorization((result) => {
            //                 if (result === PHAuthorizationStatus.Authorized) {
            //                     let filePath: string = path.join(knownFolders.documents().path, "PS_" + that.bookingID + ".jpeg");
            //                     var file: File;
            //                     file = File.fromPath(filePath);
            //                     imageSource.saveToFile(filePath, 'jpeg')
            //                     that.updateImage(imageSource);
            //                     that.signatureSourcePath = filePath;
            //                 } else {
            //                     console.log("not authorized")
            //                 }
            //             });
            //         })
            //     }

            //     that.canCheck = true;
            //     that.isDrawing = false;
            // } else
            // if (that.isCarDamageSelected) {
            var index;
            that.pendingDamagesChecker().then((value) => {

                if (value != undefined) {
                    index = value + 1;

                    if (isIOS) {

                        fromAsset(imgAssest).then((imageSource) => {

                            PHPhotoLibrary.requestAuthorization((result) => {
                                if (result === PHAuthorizationStatus.Authorized) {
                                    let filePath: string = path.join(knownFolders.documents().path, that.bookingID + "" + index + ".jpeg");
                                    var file: File;
                                    file = File.fromPath(filePath);
                                    imageSource.saveToFile(filePath, 'jpeg', 100)
                                    that.damageImageSource = filePath;

                                } else {
                                    console.log("Not authorized")
                                }
                            });
                        })

                        that.damageModal.show();
                    }
                    else if (isAndroid) {

                        var removableFile = File.fromPath(imgAssest.android);
                        var file = File.fromPath("/storage/emulated/0/Valet/Pending/Damages/" + that.bookingID + "" + index + removableFile.extension)

                        fromAsset(imgAssest)
                            .then(imageSource => {
                                imageSource.saveToFile(file.path, "jpeg");
                            });

                        // that.damageModal.show();
                        that.damageImageSource = file.path;
                    }
                }
                else {
                    index = 0;

                    if (isIOS) {

                        fromAsset(imgAssest).then((imageSource) => {
                            PHPhotoLibrary.requestAuthorization((result) => {
                                if (result === PHAuthorizationStatus.Authorized) {
                                    let filePath: string = path.join(knownFolders.documents().path, that.bookingID + "" + index + ".jpeg");
                                    var file: File;
                                    file = File.fromPath(filePath);
                                    imageSource.saveToFile(filePath, 'jpeg', 100)
                                    that.damageImageSource = filePath;

                                } else {
                                    console.log("not authorized")
                                }
                            });
                        })
                    }
                    else if (isAndroid) {

                        var removableFile = File.fromPath(imgAssest.android);
                        var file = File.fromPath("/storage/emulated/0/Valet/Pending/Damages/" + that.bookingID + "" + index + removableFile.extension)


                        fromAsset(imgAssest)
                            .then(imageSource => {
                                imageSource.saveToFile(file.path, "jpg");
                            });


                        this.damageImageSource = file.path;

                    }
                }
            })
            // }
        },
            error => {
                var toasty = new Toasty(error, ToastDuration.SHORT, ToastPosition.BOTTOM).show();
            })

        // that.selectionModal.hide();
    }

    public pickImage() {
        this.isSingleMode = true;

        let context = imagepicker.create({
            mode: "single"
        });
        this.startSelection(context);
    }

    private startSelection(context) {
        let that = this;
        // that.selectionModal.hide();

        context
            .authorize()
            .then(() => {
                return context.present();
            })
            .then((selection) => {
                if (selection[0] != null && selection[0] != undefined) {

                    var imgAssest: ImageAsset = selection[0];


                    // if (that.isSignatureSelected) {

                    //     if (isAndroid) {

                    //         var removableFile = File.fromPath(imgAssest.android);

                    //         var file = File.fromPath("/storage/emulated/0/Valet/Pending/Signatures/" + "PS_" + that.bookingID + removableFile.extension);

                    //         fromAsset(imgAssest).then(imageSource => {
                    //             imageSource.saveToFile(file.path, "jpeg");
                    //             that.updateImage(imageSource)
                    //         });
                    //         that.signatureSourcePath = file.path;
                    //     }

                    //     else if (isIOS) {
                    //         fromAsset(imgAssest).then((imageSource) => {

                    //             PHPhotoLibrary.requestAuthorization((result) => {
                    //                 if (result === PHAuthorizationStatus.Authorized) {
                    //                     let filePath: string = path.join(knownFolders.documents().path, "PS_" + that.bookingID + ".jpeg");
                    //                     var file: File;
                    //                     file = File.fromPath(filePath);
                    //                     console.log("PATH:", filePath)
                    //                     that.signatureSourcePath = filePath;
                    //                     imageSource.saveToFile(filePath, 'jpeg', 100)
                    //                     that.updateImage(imageSource)
                    //                 } else {
                    //                     console.log("not authorized")
                    //                 }
                    //             });
                    //         })
                    //     }

                    //     that.canCheck = true;
                    //     that.isDrawing = false;
                    // } else if (that.isCarDamageSelected) {

                    var index;
                    setTimeout(() => {
                        that.captureButtonShow = false;
                    }, 50);
                    that.pendingDamagesChecker().then(async (value) => {

                        if (value != undefined) {

                            index = value + 1;
                            if (isIOS) {
                                var filePath: string;

                                await fromAsset(imgAssest).then(async (imageSource) => {
                                    await PHPhotoLibrary.requestAuthorization((result) => {
                                        if (result === PHAuthorizationStatus.Authorized) {
                                            filePath = path.join(knownFolders.documents().path, that.bookingID + "" + index + ".jpeg");
                                            var file: File;
                                            file = File.fromPath(filePath);
                                            let hasSaved = imageSource.saveToFile(filePath, 'jpeg', 100)
                                            console.log('HasSaved:', hasSaved);
                                        } else {
                                            console.log("not authorized")
                                        }
                                    });
                                })
                                console.log('Before show:');
                                // that.damageModal.show();
                                that.damageImageSource = filePath;
                            }
                            else if (isAndroid) {
                                var removableFile = File.fromPath(imgAssest.android);

                                var file = File.fromPath("/storage/emulated/0/Valet/Pending/Damages/" + that.bookingID + "" + index + removableFile.extension)
                                fromAsset(imgAssest)
                                    .then(imageSource => {
                                        imageSource.saveToFile(file.path, "jpg");
                                    });

                                // that.damageModal.show();
                                that.damageImageSource = file.path;
                            }

                        }
                        else {
                            index = 0;
                            if (isIOS) {
                                var filePath: string;
                                fromAsset(imgAssest).then((imageSource) => {

                                    PHPhotoLibrary.requestAuthorization((result) => {
                                        if (result === PHAuthorizationStatus.Authorized) {
                                            filePath = path.join(knownFolders.documents().path, that.bookingID + "" + index + ".jpeg");
                                            var file: File;
                                            file = File.fromPath(filePath);
                                            imageSource.saveToFile(filePath, 'jpeg', 100);

                                            var damage = that.damageModal as ModalComponent;
                                            // console.log(damage)
                                            // damage.show();
                                            that.damageImageSource = filePath;
                                        } else {
                                            console.log("not authorized")
                                        }
                                    });
                                })


                            }
                            else if (isAndroid) {
                                var removableFile = File.fromPath(imgAssest.android);
                                var file = File.fromPath("/storage/emulated/0/Valet/Pending/Damages/" + that.bookingID + "" + index + removableFile.extension)

                                fromAsset(imgAssest)
                                    .then(imageSource => {
                                        imageSource.saveToFile(file.path, "jpg");
                                    });

                                // that.damageModal.show();
                                that.damageImageSource = file.path;
                            }
                        }
                    }).catch(function (e) {
                        console.log(e);
                    });
                }
                // }
            });
    }

    //Check Related
    checkInOut = async () => {

        let that = this;
        // that.pad = that.drawingPad.nativeElement;

        if (this.canCheck) {

            if (this.recordedOn == "") {
                console.log("recordedOn:Empty")
                return;
            }

            if (this.bookingID == 0 || this.bookingID == undefined) {
                var toasty = new Toasty("Booking id not found", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
                return;
            }

            if (that.signatureSourcePath == "" || that.signatureSourcePath == null) {
                var toasty = new Toasty("Signatures not found", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
                return
            }

            var tempSigImgSrc = fromFile(that.signatureSourcePath)

            if (tempSigImgSrc == null || tempSigImgSrc == undefined) {
                console.log("Could get Signatures from device, Please try again");
                var toasty = new Toasty("Could get Signatures from device, Please try again", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
                return;
            }

            var checkInOutPostModel = new CheckInOutPost();
            checkInOutPostModel.checking_status = this.recordedOn;

            if (this.connectionService.isConnected) {

                checkInOutPostModel.signature = "data:image/jpeg;base64," + tempSigImgSrc.toBase64String("jpeg");

                let headers = new HttpHeaders({
                    "Content-Type": "application/json",
                    "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, ""),
                });

                this.apiService.isExternalAgentUploading = true;

                await this.http.post(`${Values.BASE_URL}/api/bookings/${this.bookingID}/${checkInOutPostModel.checking_status}`, checkInOutPostModel, { headers: headers })
                    .subscribe((result: any) => {
                        console.log("res:sucees", result)

                        var temp = [];
                        var storageString = Values.readString(Values.PENDING_DAMAGES, "");

                        if (storageString != "") {
                            var arr = [];
                            arr = JSON.parse(storageString);

                            for (var i = 0; i < arr.length; i++) {
                                temp.push(arr[i])
                            }

                            for (var i = 0; i < arr.length; i++) {
                                if (arr[i].booking_id == this.bookingID) {
                                    var index = temp.indexOf(arr[i])

                                    if (index > -1) {
                                        temp.splice(index, 1);
                                    }
                                }
                            }
                        }
                        else {
                            temp = [];
                        }

                        Values.writeString(Values.PENDING_DAMAGES, JSON.stringify(temp))
                        this.apiService.postDamageObjects(this.http, this.damageImageSrcs, this.bookingID);
                        this.routerExtensions.navigate(['/dashboard']);

                    }, error => {
                        console.log("res:error", error)

                        if (this.signatureSourcePath != "") {
                            checkInOutPostModel.signature = this.signatureSourcePath;
                            checkInOutPostModel.booking_id = this.bookingID;
                            checkInOutPostModel.checking_status = this.recordedOn;

                            var tempPendingCheckString = Values.readString(Values.PENDING_CHECKS, "")
                            var tempPendingChecks = [];

                            if (tempPendingCheckString == "" || tempPendingCheckString == undefined) {
                                tempPendingChecks.push(checkInOutPostModel)
                            }
                            else {
                                tempPendingChecks = JSON.parse(tempPendingCheckString)
                                tempPendingChecks.push(checkInOutPostModel)
                            }

                            Values.writeString(Values.PENDING_CHECKS, JSON.stringify(tempPendingChecks))

                            var toasty = new Toasty("Could not check. Saved in offlines. You would be checked in automatically, once connected", ToastDuration.SHORT, ToastPosition.BOTTOM).show();

                            if (error.error.message == 'Unauthenticated.') {
                                alert('Session Expired, Please login again')
                                setTimeout(() => {
                                    Values.writeString(Values.AUTHERIZATION_TOKEN, '');
                                    this.routerExtensions.navigate(['/login'])
                                }, 1000)
                            } else {
                                this.routerExtensions.navigate(['/dashboard']);
                            }
                        }
                        this.apiService.isExternalAgentUploading = false;
                    });

            } else {

                console.log("Internet is not working")

                if (this.signatureSourcePath != "") {
                    checkInOutPostModel.signature = this.signatureSourcePath;
                    checkInOutPostModel.booking_id = this.bookingID;
                    checkInOutPostModel.checking_status = this.recordedOn;

                    var tempPendingCheckString = Values.readString(Values.PENDING_CHECKS, "")
                    var tempPendingChecks = [];

                    if (tempPendingCheckString == "" || tempPendingCheckString == undefined) {
                        tempPendingChecks.push(checkInOutPostModel)
                    }
                    else {
                        tempPendingChecks = JSON.parse(tempPendingCheckString)
                        tempPendingChecks.push(checkInOutPostModel)
                    }

                    Values.writeString(Values.PENDING_CHECKS, JSON.stringify(tempPendingChecks))

                    var toasty = new Toasty("Internet is not working. Saved in offlines", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
                    this.routerExtensions.navigate(['/dashboard']);
                }
                this.apiService.isExternalAgentUploading = false;
            }
        }
    }

    // public clearMyDrawing() {

    //     this.pad.clearDrawing();
    //     var file: File;

    //     if (this.signatureSourcePath != "") {

    //         file = File.fromPath(this.signatureSourcePath)

    //         if (file != undefined && file != null) {
    //             file.remove().then((res) => {
    //                 console.log("File:", file, "Deleted")
    //             }, error => {
    //                 console.log("Could not delete the file, Error:", error)
    //             });
    //         }
    //         this.signatureSourcePath = "";
    //     }

    //     this.canCheck = false;
    //     this.isDrawing = true;
    //     this.signatureSource = new ImageSource();
    // }

    onCancelWelcome() {
        this.welcomeModal.hide();
        this.upgradeListModel.show();
    }

    onUpgradeListCancel() {
        this.upgradeListModel.hide();
    }

    navigateBack() {
        this.routerExtensions.back();
    }

    ngOnDestroy() {
        clearTimeout(this.renderViewTimeout);
    }



    // <--------------------new Design --------------------------->
    refreshTheme() {
        let Theme = Values.readString(Values.APP_THEME, "");
        if (Theme) {
            this.appTheme = JSON.parse(Theme);
            if (this.appTheme && this.appTheme.theme && this.appTheme.theme.colors) {
                this.redColor = this.appTheme.theme.colors.one;
                this.blueColor = this.appTheme.theme.colors.two;
                this.grayColor = this.appTheme.theme.colors.bg1;
                this.whiteColor = this.appTheme.theme.colors.bg2;
            }
            else {
                this.redColor = '#f72231';
                this.blueColor = '#1896d2';
                this.grayColor = '#4C4C4C';
                this.whiteColor = '#eaeaea';
            }
            if (this.appTheme && this.appTheme.theme.logo) {
                this.appLogo = this.appTheme.theme.logo["475x475"]
                console.log("Image::", this.appLogo)
            }
        }
        else {
            this.blueColor = "#1896d2";
            this.grayColor = "#4C4C4C";
        }
    }

    onReserviationTap() {
        this.showCarsDetailsCard = false;
        this.showAddDamageCard = false;
        this.showServicesCard = false;
        this.showDamageAndsignatureCard = false;
        this.showSignatureCard = true;
    }
    onServicesTap() {
        this.showAddDamageCard = false;
        this.showCarsDetailsCard = false;
        this.showSignatureCard = false;
        this.showDamageAndsignatureCard = false;
        this.showServicesCard = true;
    }
    onAddDamageTap() {
        this.showCarsDetailsCard = false;
        this.showSignatureCard = false;
        this.showServicesCard = false;
        this.showDamageAndsignatureCard = false;
        this.showAddDamageCard = true;
    }

    oncheckInOutTap() {
        this.showAddDamageCard = false;
        this.showCarsDetailsCard = false;
        this.showSignatureCard = false;
        this.showServicesCard = false;
        this.showDamageAndsignatureCard = true;
    }

    onDestroyScreen() {
        if (this.showCarsDetailsCard == true) {
            this.routerExtensions.navigate(["/dashboard"]);
        } else {
            this.showSignatureCard = false;
            this.showAddDamageCard = false;
            this.showServicesCard = false;
            this.showDamageAndsignatureCard = false;
            this.showCarsDetailsCard = true;
        }
    }
    onCanceldamagepositionScreen() {
        this.captureButtonShow = true;
    }
    onClearSignature() {
        this.onReserviationTap();
    }

    promoTextField(args) {
        var textField = <TextField>args.object;
        this.promoCode = textField.text;
    }
    nameTextField(args) {
        var textField = <TextField>args.object;
        this.userName = textField.text;
    }
    emailTextField(args) {
        var textField = <TextField>args.object;
        this.email = textField.text;
    }
    mobileTextField(args) {
        var textField = <TextField>args.object;
        this.mobile = textField.text;
    }
    modelTextField(args) {
        var textField = <TextField>args.object;
        this.CarModel = textField.text;
    }
    plateTextField(args) {
        var textField = <TextField>args.object;
        this.carPlate = textField.text;
    }
    carkmTextField(args) {
        var textField = <TextField>args.object;
        this.carKm = textField.text;
    }
    parkingTextField(args) {
        var textField = <TextField>args.object;
        this.parkingSpace = textField.text;
    }
    flightTextField(args) {
        var textField = <TextField>args.object;
        this.flightNo = textField.text;
    }

    onColorSelect(i: number, data) {
        this.carColor = data;
        this.selectedColorIndex = i;
    }
    onUpdateCarDetails() {
        let headers = new HttpHeaders({
            "Content-Type": "application/json",
            "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, "")
        });
        if (this.userName == "" || this.mobile == "" || this.email == "" || this.flightNo == "" || this.carPlate == "" || this.parkingSpace == "") {
            alert("all fields are required")
            return;
        } else if (!this.email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
            alert("Invalid Email type")
            return;
        }
        else if (this.carColor == "") {
            alert("Please select car color")
            return;
        } else {
            this.isRendering = true;

            let body = {
                airport_id: 6299601,
                has_arrival: true,
                user_info: {
                    name: this.userName,
                    email: this.email,
                    mobile_no: this.mobile,
                },
                departure: {
                    departure_at: this.bookingModel.departure.departure_at,
                    flight_number: this.flightNo,
                },
                arrival: {
                    arrival_at: this.bookingModel.arrival.arrival_at,
                    flight_number: this.flightNo,
                },
                car_info: {
                    color: this.carColor,
                    license_plate: this.carPlate,
                    model: this.CarModel
                },
                // optionalServices: {},
                parking_service: {
                    parking_service: this.parkingSpace,

                }
            }
            this.http.put(Values.BASE_URL + `/api/bookings/${this.bookingID}`, body, { headers: headers }).subscribe((res: any) => {
                console.trace(res)
                this.isRendering = false;
                alert("sucessfully Updated")
            },
                error => {
                    console.trace('Error:::', error)
                    alert("Error ::" + error)

                    setTimeout(() => {
                        this.isRendering = false;
                    }, 10)

                    if (error.error.message == 'Unauthenticated.') {
                        alert('Session Expired, Please login again')
                        setTimeout(() => {
                            Values.writeString(Values.AUTHERIZATION_TOKEN, '');
                            this.routerExtensions.navigate(['/login'])
                        }, 400)
                    }
                })
        }
    }


}