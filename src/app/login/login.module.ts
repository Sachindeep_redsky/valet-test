import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { LoginRoutingModule } from "./login-routing.module";
import { LoginComponent } from "./components/login.component";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NgModalModule } from "../modals/ng-modal";

@NgModule({
    imports: [
        LoginRoutingModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NgModalModule
    ],
    declarations: [
        LoginComponent
    ], 

    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class LoginModule { }
