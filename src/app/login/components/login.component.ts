import { Component, ViewChild, OnInit, OnDestroy, ElementRef } from "@angular/core";
import { User } from "~/app/models/user.model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { TextField } from "tns-core-modules/ui/text-field";
import { RouterExtensions } from "nativescript-angular/router";
import { Values } from "~/app/values/values";
import { ApiService } from '~/app/services/api.service';
import { AndroidActivityBackPressedEventData, AndroidApplication } from 'tns-core-modules/application/application';
import { isAndroid, isIOS } from 'tns-core-modules/platform/platform';
import { EventData, Page } from "tns-core-modules/ui/page/page";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout/stack-layout";
import { LogGuardService } from "~/app/services/log.guard.service";
import { NavigationService } from "~/app/services/navigation.service";
import { ModalComponent } from "~/app/modals/modal.component";
import { Toasty, ToastDuration, ToastPosition } from "nativescript-toasty";
import { ObservableArray, ChangedData } from "tns-core-modules/data/observable-array";
import { FilterableListpicker } from "nativescript-filterable-listpicker";

import * as fs from "tns-core-modules/file-system/file-system"
import * as application from "tns-core-modules/application/application";

@Component({
  selector: "app-login",
  moduleId: module.id,
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit, OnDestroy {

  @ViewChild('tanentDialog', { static: false }) tenantModal: ModalComponent;
  @ViewChild('airlinesDialog', { static: false }) airlinesDialog: ModalComponent;
  @ViewChild('myfilter', { static: false }) listPicker: ElementRef;
  @ViewChild('dialogContainer', { static: false }) dialogContainer: ElementRef;

  user: User;
  emailText: string;
  passwordText: string;
  pageRef: Page;
  actions;
  locations = new ObservableArray();
  emailElement;
  passwordElement;
  selectedAirline: any;
  isRendering: boolean;
  renderViewTimeout;
  isListpicker: boolean;
  dialogHeading: string;
  showListButton: boolean;
  tanentName: string;
  // <-----------------------new veriable----------------------------------------->
  appLogo: string;
  backgroundImage: string;
  appTheme: any;
  redColor: string;
  blueColor: string;
  grayColor: string;
  whiteColor: string;
  // <-----------------------new veriable----------------------------------------->
  constructor(private http: HttpClient, private routerExtensions: RouterExtensions, private apiService: ApiService, private logGuardService: LogGuardService, private navigationService: NavigationService) {
    this.navigationService.setCurrentPage("Login")
    this.refreshTheme()
    this.appLogo = Values.readString(Values.APP_LOGO, "")
    this.backgroundImage = Values.readString(Values.APP_BACKGROUND_IMAGE, "")

    this.user = new User();
    this.emailText = "";
    this.passwordText = "";
    this.actions = [];
    this.actions.push("Prinweb");
    this.actions.push("Prinweb@");
    this.selectedAirline = { "name": "Select Airlines" }
    this.isRendering = false;
    this.tanentName = "";
    this.isListpicker = false;
    this.dialogHeading = "Select Airlines"
    console.log("doc", fs.knownFolders.documents())
    console.log("temp", fs.knownFolders.temp())
    console.log("cur", fs.knownFolders.currentApp())
    if (Values.readString(Values.SELECTED_AIRLINE, '') != "") {
      this.selectedAirline = JSON.parse(Values.readString(Values.SELECTED_AIRLINE, ''))
    }

    // this.airlinesList = new ObservableArray();
    // this.airlinesList.push({
    //   "title": "Brown Bear",

    // })

  }


  public emailTextField(args) {
    var textField = <TextField>args.object;
    this.emailText = textField.text;
  }

  public passwordTextField(args) {
    var textField = <TextField>args.object;
    this.passwordText = textField.text;
  }

  public tenantTextField(args) {
    var textField = <TextField>args.object;
    this.tanentName = textField.text;
  }

  ngOnInit(): void {

    if (isAndroid) {
      application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        data.cancel = false; // prevents default back button behavior
        this.routerExtensions.back();

      });
    }

  }

  onPageLoaded(args: EventData) {
    this.pageRef = args.object as Page;
    var menu = this.pageRef.getViewById("menuButton") as StackLayout;
    // this.airlinesDialog.show();

  }


  // onEmailTextFieldLoaded(args: any) {
  //   this.emailElement = <TextField>args.object;
  // }


  onPasswordTextFieldLoaded(args: any) {
    this.passwordElement = <TextField>args.object;
  }


  onEmailReturnPress(args) {
    var did = this.passwordElement.focus();
  }

  public openMenu() {
    // Menu.popup({
    //   view: this.pageRef.getViewById("menuButton"),
    //   actions: this.actions
    // }).then((value: any) => {
    //   console.log("Value:", value)
    //   var tempValue;

    //   console.log("Type:", typeof (value))

    //   if (typeof (value) == 'string') {
    //     tempValue = value;
    //   } else if (typeof (value) == 'object') {
    //     if (value != undefined && value.title != undefined) {
    //       tempValue = value.title;
    //     }
    //   }


    //   if (tempValue == this.actions[0]) {

    //   }
    //   else if (tempValue == this.actions[1]) {

    //   }
    // });
    this.tenantModal.show();
  }

  pickTanent() {
    // Values.writeString(Values.TENANT_NAME, this.tanentName);
    this.tenantModal.hide();

    if (this.tanentName == '') {
      var toasty = new Toasty("Tenant can not be empty", ToastDuration.SHORT, ToastPosition.BOTTOM).show();
      return;
    }
    var toasty = new Toasty("Tenant set to: " + this.tanentName, ToastDuration.SHORT, ToastPosition.BOTTOM).show();
    Values.BASE_URL = this.tanentName;
  }


  onLoginTap() {

    //UNCOMMENT FOR DYNAMIC DATA

    if (this.emailText == "") {
      alert("email cannot be empty");
      return;
    }
    if (this.passwordText == "") {
      alert("password cannot be empty");
      return;
    }
    this.isRendering = true;
    this.user.email = this.emailText;
    this.user.password = this.passwordText;

    //UNCOMMENT FOR DEVELOPMENT PURPOSES
    // this.user.email = "sachindeep@prinweb.com";
    // this.user.password = "password"

    this.apiService.postData(this.http, Values.BASE_URL + "/api/login", this.user).then((res: any) => {
      console.log("REs:", res)
      if (!(res.access_token == "")) {
        let autherization_token: any;
        autherization_token = res.access_token;
        Values.writeString(Values.AUTHERIZATION_TOKEN, autherization_token);
        this.logGuardService.logIn();
        this.getLocations();
        this.isRendering = false;

      } else {
        this.isRendering = false;
        console.trace(res.error)
      }
    },
      error => {
        console.trace("Error:", error)
        this.isRendering = false;
        alert('Login failed')
      })

  }

  getLocations() {
    let headers = new HttpHeaders({
      "Content-Type": "application/json",
      "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, "")
    });
    this.http.get(Values.BASE_URL + "/api/locations", {
      headers: headers
    }).subscribe((res: any) => {
      if (res.data.length > 0) {
        console.log('Location::', res)
        if (res.data.length == 1) {
          Values.writeString(Values.SELECTED_LOCATION, JSON.stringify(res.data[0]));
          this.routerExtensions.navigate(["/dashboard"]);
        } else {
          this.locations = res.data;
        }

        // for (let i = 0; i < res.data.length; i++) {
        //   this.airlinesList.push({
        //     "title": res.data[i].name,
        //     description: " ",
        //     id: res.data[i].id,
        //     country_code: res.data[i].country_code,
        //     country_name: res.data[i].country_name
        //   });
        // }

        // let locations = [];
        // for (let j = 0; j < this.airlinesList.length; j++) {
        //   for (let k = 0; k < locations.length; k++) {
        //     if (!(this.airlinesList[j].country_name == locations[k].country_name)) {
        //       locations.push(this.airlinesList[j])
        //     }
        //   }
        // }
        // this.isListpicker = true;
      }
      // console.log(this.airlinesList);

      this.airlinesDialog.show();

    })
  }
  onTenantButtonTap() {
    this.routerExtensions.navigate(["/tenant"]);
  }

  refreshTheme() {
    let Theme = Values.readString(Values.APP_THEME, "");
    if (Theme) {
      this.appTheme = JSON.parse(Theme);
      if (this.appTheme && this.appTheme.theme && this.appTheme.theme.colors) {
        this.redColor = this.appTheme.theme.colors.one;
        this.blueColor = this.appTheme.theme.colors.two;
        this.grayColor = this.appTheme.theme.colors.bg1;
        this.whiteColor = this.appTheme.theme.colors.bg2;
      }
      else {
        this.redColor = '#f72231';
        this.blueColor = '#1896d2';
        this.grayColor = '#4C4C4C';
        this.whiteColor = '#eaeaea';
      }
    } else {
      this.redColor = '#E04940';
      this.blueColor = '#1896d2';
    }
  }


  ngOnDestroy() {
    clearTimeout(this.renderViewTimeout);
  }

  onAirlineDone() {
    this.airlinesDialog.hide()
    this.routerExtensions.navigate(["/dashboard"]);
  }

  onAirlinesCancel() {
    this.airlinesDialog.hide()
  }

  showList() {
    this.isListpicker = true;
  }

  showListPiker() {
    this.listPicker.nativeElement.show(this.dialogContainer.nativeElement);
  }

  onListLoaded(args) {
    var list = <FilterableListpicker>args.object;
    list.on(FilterableListpicker.itemTappedEvent, (args: any) => {
      console.log('TappedLLLL', args.selectedItem)
    })
  }

  onTap(args) {
    console.log('Tapped1', args.selectedItem)
  }

  airlinesListitemTapped(args: any, item) {
    console.log('Tapped2') 
    this.selectedAirline = item
    Values.writeString(Values.SELECTED_LOCATION, JSON.stringify(item))
    this.isListpicker = false;
  }

  onListDone() {
    if (this.selectedAirline.name != "Select Airlines") {
      this.airlinesDialog.hide()
      this.routerExtensions.navigate(["/dashboard"]);
    }
  }
}
