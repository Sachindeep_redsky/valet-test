import { Component } from "@angular/core";
import { Page } from "tns-core-modules/ui/page/page";
import { TextField } from "tns-core-modules/ui/text-field/text-field";
import { User } from "~/app/models/user.model";
import { Departure } from "~/app/models/departure.model";
import { Arrival } from "~/app/models/arrival.model";
import { CarInfo } from "~/app/models/car-info.model";
import { OptionalServices } from "~/app/models/optionalservices.model";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Values } from "~/app/values/values";
import { Booking } from "~/app/models/booking.model";
import { NavigationService } from "~/app/services/navigation.service";
import { ApiService } from '~/app/services/api.service';
import { isAndroid, isIOS } from 'tns-core-modules/platform/platform';
import { RouterExtensions } from "nativescript-angular/router";
import * as application from "tns-core-modules/application/application";



@Component({
  selector: "app-create-booking",
  moduleId: module.id,
  templateUrl: "./createbooking.component.html",
  styleUrls: ["./createbooking.component.css"]
})

export class CreateBookingComponent {

  page: Page;

  bookingModel = new Booking();

  airportId: number;
  hasArrival: boolean;
  airlineIdDep: number;
  departureAt: string;
  flightNumberDep: string;
  airlineIdAri: number;
  arrivalAt: string;
  flightNumberAri: string;
  carBrandId: number;
  carLicencePlate: string;

  booking = true;
  // < -----------------------New Design-------------------------------------->

  appLogo: string;
  appTheme: any;
  redColor: string;
  blueColor: string;
  grayColor: string;
  whiteColor: string;
  colors: any[];
  selectedColorIndex: number;
  promoCode: string;
  firstName: string;
  email: string;
  mobile: string;
  CarModel: string;
  carPlate: string;
  carColor: string;
  carKm: string;
  parkingSpace: string;
  flightNo: string;
  arrivalDate: string;
  arriveTime: string;
  depatureDate: string;
  DepartureTime: string;
  isRendering: boolean;
  // < -----------------------New Design-------------------------------------->


  constructor(private http: HttpClient, private routerExtensions: RouterExtensions, private apiService: ApiService, private navigationService: NavigationService) {
    this.navigationService.setCurrentPage("newBooking")
    this.refreshTheme()
    this.colors = ["#000000", "#c0c0c0", "#ff0000", "#87ceeb", "#008000", "#e9c204", "#ffffff", "#0000ff", "#808080"]
    this.appLogo = Values.readString(Values.APP_LOGO, "")

    this.isRendering = false;
    this.promoCode = "";
    this.firstName = "";
    this.email = "";
    this.mobile = "";
    this.CarModel = "";
    this.carPlate = "";
    this.carColor = "";
    this.carKm = "";
    this.parkingSpace = "";
    this.flightNo = "";
    this.arrivalDate = "";
    this.arriveTime = "";
    this.depatureDate = "";
    this.DepartureTime = "";
  }

  ngOnInit(): void {
    if (isAndroid) {
      application.android.on(application.AndroidApplication.activityBackPressedEvent, (data: application.AndroidActivityBackPressedEventData) => {
        data.cancel = false; // prevents default back button behavior
        this.routerExtensions.navigate(["/dashboard"]);
      });
    }

  }

  onPageLoaded(args) {
    this.page = args.object as Page;
  }

  promoTextField(args) {
    var textField = <TextField>args.object;
    this.promoCode = textField.text;
  }
  nameTextField(args) {
    var textField = <TextField>args.object;
    this.firstName = textField.text;
  }
  emailTextField(args) {
    var textField = <TextField>args.object;
    this.email = textField.text;
  }
  mobileTextField(args) {
    var textField = <TextField>args.object;
    this.mobile = textField.text;
  }
  modelTextField(args) {
    var textField = <TextField>args.object;
    this.CarModel = textField.text;
  }
  plateTextField(args) {
    var textField = <TextField>args.object;
    this.carPlate = textField.text;
  }
  carkmTextField(args) {
    var textField = <TextField>args.object;
    this.carKm = textField.text;
  }
  parkingTextField(args) {
    var textField = <TextField>args.object;
    this.parkingSpace = textField.text;
  }
  flightTextField(args) {
    var textField = <TextField>args.object;
    this.flightNo = textField.text;
  }
  arriveTimeTextField(args) {
    var textField = <TextField>args.object;
    this.arrivalDate = textField.text;
  }
  arriveDateTextField(args) {
    var textField = <TextField>args.object;
    this.arriveTime = textField.text;
  }
  depatureTextField(args) {
    var textField = <TextField>args.object;
    this.depatureDate = textField.text;
  }
  depatureTimeTextField(args) {
    var textField = <TextField>args.object;
    this.DepartureTime = textField.text;
  }

  onColorSelect(i: number, data) {
    this.carColor = data;
    this.selectedColorIndex = i;
  }

  onNextClick() {
    let selectedLocation = Values.readString(Values.SELECTED_LOCATION, '');
    let locationObj;
    let query = 'airport_id=2660644';
    if (selectedLocation) {
      locationObj = JSON.parse(selectedLocation);
      query = `airport_id=${locationObj.id}`
    }

    this.bookingModel = new Booking();
    let user_info = new User();
    let departure = new Departure();
    let arrival = new Arrival();
    let car_info = new CarInfo();
    let parking_service = new OptionalServices();
    let optionalServices = new OptionalServices();

    if (this.firstName != null && this.firstName != undefined) {
      user_info.name = this.firstName;
    }
    if (this.email != null && this.email != undefined) {
      user_info.email = this.email;
    }
    if (this.mobile != null && this.mobile != undefined) {
      user_info.mobile_no = this.mobile;
    }

    if (this.CarModel != null && this.CarModel != undefined) {
      car_info.model = this.CarModel;
    }
    if (this.carPlate != null && this.carPlate != undefined) {
      car_info.license_plate = this.carPlate;
    }
    if (this.carColor != null && this.carColor != undefined) {
      car_info.color = this.carColor;
    }

    if (this.parkingSpace != null && this.parkingSpace != undefined) {
      parking_service.parking = this.parkingSpace;
    }

    if (this.flightNo != null && this.flightNo != undefined) {
      arrival.flight_number = this.flightNo;
    }
    if (this.arrivalDate != null && this.arrivalDate != undefined) {
      arrival.arrival_at = "2020-10-24T06:09:00.000000Z";
      // arrival.arrival_at = this.arrivalDate + this.arriveTime;
    }

    if (this.DepartureTime != null && this.DepartureTime != undefined) {
      // departure.departure_at = this.depatureDate + this.DepartureTime;
      departure.departure_at = "2020-10-22T06:09:00.000000Z";
      departure.flight_number = this.flightNo;
    }

    optionalServices.parking = this.parkingSpace;
    //  this.bookingModel.has_arrival = this.hasArrival;
    this.bookingModel.airport_id = query;
    this.bookingModel.has_arrival = false;
    this.bookingModel.user_info = user_info;
    this.bookingModel.departure = departure;
    this.bookingModel.arrival = arrival;
    this.bookingModel.car_info = car_info;
    this.bookingModel.parking_service = parking_service;
    this.bookingModel.optionalServices = optionalServices;
    console.log("Booking Model :::", this.bookingModel)
    // this.booking = false;

    this.onSubmitClick();
  }


  onSubmitClick() {
    if (this.firstName == "" || this.mobile == "" || this.email == "" || this.flightNo == "" || this.carPlate == "" || this.parkingSpace == "") {
      alert("all fields are required")
      return;
    } else if (!this.email.match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
      alert("Invalid Email type")
      return;
    }
    else if (this.carColor == "") {
      alert("Please select car color")
      return;
    } else {
      let headers = new HttpHeaders({
        "Content-Type": "application/json",
        "Authorization": "Bearer " + Values.readString(Values.AUTHERIZATION_TOKEN, "")
      })

      this.isRendering = true;
      this.http.post(Values.BASE_URL + "/api/bookings", this.bookingModel, { headers: headers }).subscribe((res: any) => {
        if (res.data) {
          alert("Success")
          this.booking = false;
          console.log("newBooking Res:::", res.data)
          this.isRendering = false;
        }
      }, error => {
        alert(error)
        console.log("newBooking Res:::", error)
        this.isRendering = false;
        if (error.error.message == 'Unauthenticated.') {
          alert('Session Expired, Please login again')
          setTimeout(() => {
            Values.writeString(Values.AUTHERIZATION_TOKEN, '');
            this.routerExtensions.navigate(['/login'])
          }, 400)
        }
        // alert("Error")
      })
    }
  }

  onBackClick() {
    this.booking = true;
  }

  onCloseScreen() {
    // this.routerExtensions.back();
    this.routerExtensions.navigate(["/dashboard"]);
  }

  refreshTheme() {
    let Theme = Values.readString(Values.APP_THEME, "");
    if (Theme) {
      this.appTheme = JSON.parse(Theme);
      if (this.appTheme && this.appTheme.theme && this.appTheme.theme.colors) {
        this.redColor = this.appTheme.theme.colors.one;
        this.blueColor = this.appTheme.theme.colors.two;
        this.grayColor = this.appTheme.theme.colors.bg1;
        this.whiteColor = this.appTheme.theme.colors.bg2;
      }
      else {
        this.blueColor = '#1896d2';
        this.grayColor = '#4C4C4C';
      }
    }
    else {
      this.blueColor = '#1896d2';
      this.grayColor = '#4C4C4C';
    }
  }

}