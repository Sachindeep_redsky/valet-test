import { NgModule } from "@angular/core";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { Routes } from "@angular/router";

const routes: Routes = [
  // { path: "", redirectTo: "/tenant", pathMatch: "full" },
  { path: "tenant", loadChildren: "~/app/tenant/tenant.module#TenantModule" },
  { path: "login", loadChildren: "~/app/login/login.module#LoginModule" },
  { path: "dashboard", loadChildren: "~/app/dashboard/dashboard.module#DashboardModule" },
  { path: "createbooking", loadChildren: "~/app/createbooking/createbooking.module#CreateBookingModule" },
  { path: "check", loadChildren: "~/app/check/check.module#CheckModule" },
  { path: "plate", loadChildren: "~/app/plate/plate.module#PlateModule" },
];

@NgModule({
  imports: [NativeScriptRouterModule.forRoot(routes)],
  exports: [NativeScriptRouterModule]
})
export class AppRoutingModule { }
