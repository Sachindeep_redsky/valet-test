import { Component, OnInit } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { TextField } from "tns-core-modules/ui/text-field";
import { RouterExtensions } from "nativescript-angular/router";
import { Values } from "~/app/values/values";
import { ApiService } from '~/app/services/api.service';
import { AndroidActivityBackPressedEventData, AndroidApplication } from 'tns-core-modules/application/application';
import { isAndroid, isIOS } from 'tns-core-modules/platform/platform';
import { NavigationService } from "~/app/services/navigation.service";
import * as application from "tns-core-modules/application/application";

@Component({
  selector: "tenant",
  moduleId: module.id,
  templateUrl: "./tenant.component.html",
  styleUrls: ["./tenant.component.css"]
})
export class tenantComponent implements OnInit {
  appTheme: any;
  appLogo: string;
  backgroundImage: string;
  redColor: string;
  blueColor: string;
  grayColor: string;
  whiteColor: string;

  tanentURL: string;
  isRendering: boolean;

  constructor(private http: HttpClient, private routerExtensions: RouterExtensions, private apiService: ApiService, private navigationService: NavigationService) {
    this.navigationService.setCurrentPage("TanentURL")
    this.tanentURL = (Values.BASE_URL + "/api/settings");

  }

  ngOnInit(): void {
    if (isAndroid) {
      application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
        data.cancel = true; // prevents default back button behavior
      });
    }
    if (Values.readString(Values.APP_BACKGROUND_IMAGE, "") == "") {
      this.getAppTheme(1);
    }
    else {
      this.refreshTheme();
    }

  }

  public tenantTextField(args) {
    var textField = <TextField>args.object;
    this.tanentURL = textField.text;
  }

  public getAppTheme(data) {
    this.isRendering = true;
    this.http.get(this.tanentURL).subscribe((res: any) => {
      if (res != null && res != undefined) {
        Values.writeString(Values.APP_BACKGROUND_IMAGE, res.data.theme["backgroud-image"])
        Values.writeString(Values.APP_LOGO, res.data.theme.logo["670x670"])
        Values.writeString(Values.APP_THEME, JSON.stringify(res.data))
        console.log("BackgroundImage:::", Values.readString(Values.APP_BACKGROUND_IMAGE, ""))
        this.refreshTheme();
        this.isRendering = false;
        if(data.eventName==='tap'){
          this.routerExtensions.navigate(["/login"]);
        }
      }
      else {
        this.isRendering = false;
      }
    },
      error => {
        this.isRendering = false;
        this.refreshTheme();
        alert(error);
        if(data.eventName=="tap"){
          this.routerExtensions.navigate(["/login"]);
        }
      })
  }

  refreshTheme() {
    let Theme = Values.readString(Values.APP_THEME, "");
    if (Theme) {
      this.appTheme = JSON.parse(Theme);
      if (this.appTheme) {
        if (this.appTheme.theme && this.appTheme.theme.colors) {
          this.redColor = this.appTheme.theme.colors.one;
          this.blueColor = this.appTheme.theme.colors.two;
          this.grayColor = this.appTheme.theme.colors.bg1;
          this.whiteColor = this.appTheme.theme.colors.bg2;
        }
        else {
          this.redColor = '#f72231';
          this.blueColor = '#1896d2';
          this.grayColor = '#4C4C4C';
          this.whiteColor = '#eaeaea';
        }
        if (this.appTheme.theme && this.appTheme.theme["backgroud-image"]) {
          this.backgroundImage = Values.readString(Values.APP_BACKGROUND_IMAGE, "")
        }
        else {
          this.backgroundImage = "";
        }
        if (this.appTheme.theme && this.appTheme.theme.logo) {
          this.appLogo = Values.readString(Values.APP_LOGO, "")
        }
        else {
          this.appLogo = "res://gallery_icon";
        }
      }

    }

  }

}
