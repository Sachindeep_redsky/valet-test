import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";
import { TenantRoutingModule } from "./tenant-routing.module";
import { tenantComponent } from "./components/tenant.component";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NgModalModule } from "../modals/ng-modal";

@NgModule({
    imports: [
        TenantRoutingModule,
        NativeScriptFormsModule,
        NativeScriptCommonModule,
        NativeScriptHttpClientModule,
        NgModalModule
    ],
    declarations: [
        tenantComponent
    ],

    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class TenantModule { }
