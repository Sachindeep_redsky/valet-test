import { Component, OnInit, OnDestroy, Input, AfterViewInit } from "@angular/core";

@Component({
  selector: "HeaderList",
  moduleId: module.id,
  templateUrl: "./header-list.component.html",
  styleUrls: ["./header-list.component.css"]
})
export class HeaderListComponent implements OnInit{

  @Input() items;
  @Input() height: string = '100%';
  @Input() width: string = '100%';
  selectedIndex: number;
  selectedParentIndex: number;
  cardBackground:string;
  acceptService:boolean;
  isRendering:boolean;

  constructor() {
    this.selectedIndex = 0;
    this.selectedParentIndex = 0;
    this.cardBackground = "#328BC3"
    this.acceptService = true;
    this.isRendering=  false;

  }
 
  ngOnInit(): void {
    if (this.items) {
      console.log("Opetional Dataa ::::", this.items)
      for (let i = 0; i < this.items.length; i++) {

        for (let a = 0; a < this.items[i].items.length; a++) {
          this.items[i].items[a].isSelected = false;

        }
      }
    }
  }

  onSelect(parentIndex: number, index: number) {
    this.selectedParentIndex = parentIndex;
    this.selectedIndex = index;
    for (let a = 0; a < this.items[parentIndex].items.length; a++) {
      this.items[parentIndex].items[a].isSelected = false;
    }
    this.items[parentIndex].items[index].isSelected = true;
  }

  onUpgrade() {
    if (this.items[this.selectedParentIndex].items[this.selectedIndex].isSelected) {
      this.items[this.selectedParentIndex].items[this.selectedIndex].isSelected = false;
      alert("Service Upgraded")
    } else {
      alert('Select a service')
    }
  }

  onAddService(){
    this.acceptService = false;
  }
  onSubmitClick(){
    this.isRendering=true;
    setTimeout(() => {
      this.isRendering=false;
    }, 300);
  }
}
